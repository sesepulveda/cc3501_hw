
# coding=utf-8
"""
Daniel Calderon, CC3501, 2019-1
vertices and indices for simple shapes
"""
import numpy as np
import math


# A simple class container to store vertices and indices that define a shape
class Shape:
    def __init__(self, vertices, indices, textureFileName=None):
        self.vertices = vertices
        self.indices = indices
        self.textureFileName = textureFileName


def createAxis(length=1.0):

    # Defining the location and colors of each vertex  of the shape
    vertices = [
    #    positions        colors
        -length,  0.0,  0.0, 0.0, 0.0, 0.0,
         length,  0.0,  0.0, 1.0, 0.0, 0.0,

         0.0, -length,  0.0, 0.0, 0.0, 0.0,
         0.0,  length,  0.0, 0.0, 1.0, 0.0,

         0.0,  0.0, -length, 0.0, 0.0, 0.0,
         0.0,  0.0,  length, 0.0, 0.0, 1.0]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
         0, 1,
         2, 3,
         4, 5]

    return Shape(vertices, indices)


def createRainbowTriangle():

    # Defining the location and colors of each vertex  of the shape
    vertices = [
    #   positions        colors
        -0.5, -0.5, 0.0,  1.0, 0.0, 0.0,
         0.5, -0.5, 0.0,  0.0, 1.0, 0.0,
         0.0,  0.5, 0.0,  0.0, 0.0, 1.0]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [0, 1, 2]

    return Shape(vertices, indices)


def createRainbowQuad():

    # Defining the location and colors of each vertex  of the shape
    vertices = [
    #   positions        colors
        -0.5, -0.5, 0.0,  1.0, 0.0, 0.0,
         0.5, -0.5, 0.0,  0.0, 1.0, 0.0,
         0.5,  0.5, 0.0,  0.0, 0.0, 1.0,
        -0.5,  0.5, 0.0,  1.0, 1.0, 1.0]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
        0, 1, 2,
        2, 3, 0]

    return Shape(vertices, indices)



def createCircle(color,R,n):
    vertices = [0.0, 0.0, 0.0, color[0], color[1], color[2]]
    dtheta = 2*np.pi/n
    for i in range(n):
        theta = i * dtheta
        vertices+=[R*np.cos(theta), R*np.sin(theta),0.0, 
            color[0], color[1], color[2]]

    indices = []

    for i in range(1,n):
        indices+=(0,i,i+1)
        if i==n-1: indices+=(0,n,1)

    return Shape(vertices,indices)

def createTextureCircle(file):
    R =1
    n=30
    vertices = [0.0, 0.0, 0.0]

    dtheta = 2*np.pi/n
    for i in range(n):
        theta = i * dtheta
        vertices+=[R*np.cos(theta), R*np.sin(theta),0.0,np.cos(theta),np.sin(theta)]
    indices = []

    for i in range(1,n):
        indices+=(0,i,i+1)
        if i==n-1: indices+=(0,n,1)

    return Shape(vertices,indices,file)


def __vertexUnpack3(vertex):
    """
    Extend vertex to 3 dimension.

    :param vertex:
    :return:
    """
    if len(vertex) == 2:
        vertex = vertex + (0,)
    return vertex


def create4VertexColor(p1, p2, p3, p4, r, g, b):
    """
    Creates a 4-vertex poly with color.

    :param p1: Vertex (x,y,z)
    :param p2: Vertex (x,y,z)
    :param p3: Vertex (x,y,z)
    :param p4: Vertex (x,y,z)
    :param r: Red color
    :param g: Green color
    :param b: Blue color
    :return:
    """
    # Extend
    p1 = __vertexUnpack3(p1)
    p2 = __vertexUnpack3(p2)
    p3 = __vertexUnpack3(p3)
    p4 = __vertexUnpack3(p4)

    # Dissamble vertices
    x1, y1, z1 = p1
    x2, y2, z2 = p2
    x3, y3, z3 = p3
    x4, y4, z4 = p4

    # Defining locations and color
    vertices = [
        # X, Y,  Z, R, G, B,
        x1, y1, z1, r, g, b,
        x2, y2, z2, r, g, b,
        x3, y3, z3, r, g, b,
        x4, y4, z4, r, g, b
    ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
        0, 1, 2,
        2, 3, 0
    ]

    return Shape(vertices, indices)

#########################################################################
# build vertices of cone 
# the first points are the center of the angles with distincts degres
#########################################################################
def createCone(r,g,b):
    radius = 2.0
    color = [r,g,b]
    n = 20
    #punto inferior
    vertices = [0.0, 0.0, 0.0, color[0], color[1], color[2]]
    #punto superior
    vertices += [0.0, 0.0, 3.0, color[0], color[1], color[2]]

    dtheta = 2*np.pi/n
    for i in range(n):
        theta = i * dtheta
        vertices+=[radius*np.cos(theta), radius*np.sin(theta),0.0,
        color[0], color[1], color[2]]

    indices = []

    for i in range(2,n+1):
        indices+=(0,i,i+1)
        if i==n: indices+=(0,n+1,2)

    for i in range(2,n+1):
        indices+=(1,i,i+1)
        if i==n: indices+=(1,n+1,2)

    return Shape(vertices,indices)


#########################################################################
# build vertices of cylinder 
# It is two circles
#########################################################################
def createCylinder(r,g,b,n):
    radius = 1.0
    #punto inferior
    vertices = [0.0, 0.0, 0.0, r, g, b]
    #punto superior
    vertices += [0.0, 0.0, 7.0, r, g, b]
    indices = []

    dtheta = 2*np.pi/n

    #top lid
    for i in range(n):
        theta = i * dtheta
        vertices+=[radius*np.cos(theta), radius*np.sin(theta),0.0,
        r, g, b]

    for i in range(2,n+1):
        indices+=(0,i,i+1)
        if i==n: indices+=(0,n+1,2)

    #lower cap
    for i in range(n):
        theta = i * dtheta
        vertices+=[radius*np.cos(theta), radius*np.sin(theta),7.0,
        r, g, b]

    for i in range(n+2,2*n+1):
        indices+=(1,i,i+1)
        if i==2*n: indices+=(1,2*n+1,n+2)

    for i in range(2,n+1):
        indices+=(i,n+i,n+i+1,n+i+1,i+1,i)

    #print("largo ver: "+ str(len(vertices)))
    #print("largo ind: "+ str((indices)))

    return Shape(vertices,indices)

#ppizarror's cilyndre
#latitude subdivides theta, longitude
#subdivides h
def createCylinde(r,h,lat,lon):
    #vertices = [0.0, 0.0, 0.0, rtral[0], colorCentral[1], colorCentral[2]]

    dtheta = 2*np.pi/lat
    vertices = []
    indices = []

    color = {
        'r': 1,  # Red
        'g': 0,  # Green
        'b': 0,  # Blue
    }
    cylinder = []

    for i in range(lon):  # Vertical component
        for j in range(lat):  # Horizontal component

            # Angle on step j
            theta = dtheta * j

            # Here we create a quad from 4 vertices
            #
            #    a/---- b/
            #    |      |
            #    d ---- c
            a = [r * np.cos(theta), r * np.sin(theta), h / lon * (i + 1)]
            b = [r * np.cos(theta + dtheta), r * np.sin(theta + dtheta), h / lon * (i + 1)]
            c = [r * np.cos(theta + dtheta), r * np.sin(theta + dtheta), h / lon * i]
            d = [r * np.cos(theta), r * np.sin(theta), h / lon * i]
            # Create quad
            shape = bs_ext.create4VertexColorNormal(a, b, c, d, color['r'], color['g'], color['b'])
            cylinder.append(es.toGPUShape(shape))
    return cylinder



#########################################################################
# build vertices of sphere with smooth shading using parametric equation
# x = r * cos(u) * cos(v)
# y = r * cos(u) * sin(v)
# z = r * sin(u)
# where u: stack(latitude) angle (-90 <= u <= 90)
#       v: sector(longitude) angle (0 <= v <= 360)
#########################################################################
def createSphere(radius,stackCount,sectorCount,cl):
    #x, y, z, xy                              # vertex position
    #nx, ny, nz, lengthInv = 1.0 / radius    # normal
    #s, t                                     # texCoord
    lengthInv = 1.0 / radius
    vertices =[]
    normales =[]
    texCoord =[]
    sectorStep = 2 * math.pi / sectorCount
    stackStep = math.pi / stackCount
    #sectorAngle, stackAngle
    #for(int i = 0 i <= stackCount ++i)
    for i in range(stackCount+1): 
        stackAngle = math.pi / 2 - i * stackStep        # starting from pi/2 to -pi/2
        xy = radius * np.cos(stackAngle)             # r * cos(u)
        z = -radius * np.sin(stackAngle)              # r * sin(u)

        # add (sectorCount+1) vertices per stack
        # the first and last vertices have same position and normal, but different tex coords
        # for(int j = 0 j <= sectorCount ++j):
        for j in range(sectorCount+1):

            sectorAngle = j * sectorStep           # starting from 0 to 2pi

            # vertex position
            x = xy * math.cos(sectorAngle)             # r * cos(u) * cos(v)
            y = xy * math.sin(sectorAngle)             # r * cos(u) * sin(v)
            vertices += [x,y,z,cl[0],cl[1],cl[2]]

            # normalized vertex normal
            nx = x * lengthInv
            ny = y * lengthInv
            nz = z * lengthInv
            normales += [nx, ny, nz]

            # vertex tex coord between [0, 1]
            s = 1.0*j / sectorCount
            t = 1.0*i / stackCount
            texCoord+=[s,t]

    # indices
    #  k1--k1+1
    #  |  / |
    #  | /  |
    #  k2--k2+1
    indices = []
    lineIndices = []
    #unsigned int k1, k2
    for i in range(stackCount):
        k1 = i * (sectorCount + 1)     # beginning of current stack
        k2 = k1 + sectorCount + 1      # beginning of next stack

        #(int j = 0 j < sectorCount ++j, ++k1, ++k2)
        for j in range(sectorCount):
            # 2 triangles per sector excluding 1st and last stacks
            if(i != 0):
                indices.append(k1)
                indices.append(k2)
                indices.append(k1+1)   # k1---k2---k1+1

            if(i != (stackCount-1)):
                indices.append(k1+1)
                indices.append(k2)
                indices.append(k2+1)   # k1+1---k2---k2+1

            # vertical lines for all stacks
            lineIndices.append(k1)
            lineIndices.append(k2)
            if(i != 0):
                lineIndices.append(k1)
                lineIndices.append(k1 + 1)
            k1+=1
            k2+=1

    #print("largo ver: "+ str(len(vertices)))
    #print("largo ind: "+ str(len(indices)))
    #print("vert texCoord: "+ str(texCoord))

    return Shape(vertices, indices)


def createSphereTexture(image_filename,radius):
    stackCount=20
    sectorCount=20
    textureFileName = image_filename
    #x, y, z, xy                              # vertex position
    #nx, ny, nz, lengthInv = 1.0 / radius    # normal
    #s, t                                     # texCoord
    lengthInv = 1.0 / radius
    vertices =[]
    normales =[]
    texCoord =[]
    sectorStep = 2 * math.pi / sectorCount
    stackStep = math.pi / stackCount
    #sectorAngle, stackAngle
    #for(int i = 0 i <= stackCount ++i)
    for i in range(stackCount+1): 
        stackAngle = math.pi / 2 - i * stackStep        # starting from pi/2 to -pi/2
        xy = radius * np.cos(stackAngle)             # r * cos(u)
        z = -radius * np.sin(stackAngle)              # r * sin(u)

        # add (sectorCount+1) vertices per stack
        # the first and last vertices have same position and normal, but different tex coords
        # for(int j = 0 j <= sectorCount ++j):
        for j in range(sectorCount+1):

            sectorAngle = j * sectorStep           # starting from 0 to 2pi

            # vertex position
            x = xy * math.cos(sectorAngle)             # r * cos(u) * cos(v)
            y = xy * math.sin(sectorAngle)             # r * cos(u) * sin(v)
            vertices += [x,y,z]

            # normalized vertex normal
            nx = x * lengthInv
            ny = y * lengthInv
            nz = z * lengthInv
            normales += [nx, ny, nz]

            # vertex tex coord between [0, 1]
            s = 1.0*j / sectorCount
            t = 1.0*i / stackCount
            vertices+=[s,t]

    # indices
    #  k1--k1+1
    #  |  / |
    #  | /  |
    #  k2--k2+1
    indices = []
    lineIndices = []
    #unsigned int k1, k2
    for i in range(stackCount):
        k1 = i * (sectorCount + 1)     # beginning of current stack
        k2 = k1 + sectorCount + 1      # beginning of next stack

        #(int j = 0 j < sectorCount ++j, ++k1, ++k2)
        for j in range(sectorCount):
            # 2 triangles per sector excluding 1st and last stacks
            if(i != 0):
                indices.append(k1)
                indices.append(k2)
                indices.append(k1+1)   # k1---k2---k1+1

            if(i != (stackCount-1)):
                indices.append(k1+1)
                indices.append(k2)
                indices.append(k2+1)   # k1+1---k2---k2+1

            # vertical lines for all stacks
            lineIndices.append(k1)
            lineIndices.append(k2)
            if(i != 0):
                lineIndices.append(k1)
                lineIndices.append(k1 + 1)
            k1+=1
            k2+=1

    #print("largo ver: "+ str(len(vertices)))
    #print("largo ind: "+ str(len(indices)))
    #print("vert texCoord: "+ str(texCoord))

    return Shape(vertices, indices, textureFileName)


def createColorNormalsSphere(r,g,b):
    radius=1
    stackCount=20
    sectorCount=20
    #x, y, z, xy                              # vertex position
    #nx, ny, nz, lengthInv = 1.0 / radius    # normal
    #s, t                                     # texCoord
    lengthInv = 1.0 / radius
    vertices =[]
    normales =[]
    texCoord =[]
    sectorStep = 2 * math.pi / sectorCount
    stackStep = math.pi / stackCount
    #sectorAngle, stackAngle
    #for(int i = 0 i <= stackCount ++i)
    for i in range(stackCount+1): 
        stackAngle = math.pi / 2 - i * stackStep        # starting from pi/2 to -pi/2
        xy = radius * np.cos(stackAngle)             # r * cos(u)
        z = -radius * np.sin(stackAngle)              # r * sin(u)

        # add (sectorCount+1) vertices per stack
        # the first and last vertices have same position and normal, but different tex coords
        # for(int j = 0 j <= sectorCount ++j):
        for j in range(sectorCount+1):

            sectorAngle = j * sectorStep           # starting from 0 to 2pi

            # vertex position
            x = xy * math.cos(sectorAngle)             # r * cos(u) * cos(v)
            y = xy * math.sin(sectorAngle)             # r * cos(u) * sin(v)
            vertices += [x,y,z,r,g,b]

            # normalized vertex normal
            nx = x * lengthInv
            ny = y * lengthInv
            nz = z * lengthInv
            vertices += [nx, ny, nz]

            # vertex tex coord between [0, 1]
            s = 1.0*j / sectorCount
            t = 1.0*i / stackCount
            texCoord+=[s,t]

    # indices
    #  k1--k1+1
    #  |  / |
    #  | /  |
    #  k2--k2+1
    indices = []
    lineIndices = []
    #unsigned int k1, k2
    for i in range(stackCount):
        k1 = i * (sectorCount + 1)     # beginning of current stack
        k2 = k1 + sectorCount + 1      # beginning of next stack

        #(int j = 0 j < sectorCount ++j, ++k1, ++k2)
        for j in range(sectorCount):
            # 2 triangles per sector excluding 1st and last stacks
            if(i != 0):
                indices.append(k1)
                indices.append(k2)
                indices.append(k1+1)   # k1---k2---k1+1

            if(i != (stackCount-1)):
                indices.append(k1+1)
                indices.append(k2)
                indices.append(k2+1)   # k1+1---k2---k2+1

            # vertical lines for all stacks
            lineIndices.append(k1)
            lineIndices.append(k2)
            if(i != 0):
                lineIndices.append(k1)
                lineIndices.append(k1 + 1)
            k1+=1
            k2+=1

    #print("largo ver: "+ str(len(vertices)))
    #print("largo ind: "+ str(len(indices)))
    #print("vert texCoord: "+ str(texCoord))

    return Shape(vertices, indices)


def createNormalsTextureSphere(image_filename):
    radius=0.7
    stackCount=60
    sectorCount=60
    #x, y, z, xy                              # vertex position
    #nx, ny, nz, lengthInv = 1.0 / radius    # normal
    #s, t                                     # texCoord
    lengthInv = 1.0 / radius
    vertices =[]
    normales =[]
    texCoord =[]
    sectorStep = 2 * math.pi / sectorCount
    stackStep = math.pi / stackCount
    #sectorAngle, stackAngle
    #for(int i = 0 i <= stackCount ++i)
    for i in range(stackCount+1): 
        stackAngle = math.pi / 2 - i * stackStep        # starting from pi/2 to -pi/2
        xy = radius * np.cos(stackAngle)             # r * cos(u)
        z = -radius * np.sin(stackAngle)              # r * sin(u)

        # add (sectorCount+1) vertices per stack
        # the first and last vertices have same position and normal, but different tex coords
        # for(int j = 0 j <= sectorCount ++j):
        for j in range(sectorCount+1):

            sectorAngle = j * sectorStep           # starting from 0 to 2pi

            # vertex position
            x = xy * math.cos(sectorAngle)             # r * cos(u) * cos(v)
            y = xy * math.sin(sectorAngle)             # r * cos(u) * sin(v)
            vertices += [x,y,z]
            # vertex tex coord between [0, 1]
            s = 1.0*j / sectorCount
            t = 1.0*i / stackCount
            vertices+=[s,t]              

            # normalized vertex normal
            nx = x * lengthInv
            ny = y * lengthInv
            nz = z * lengthInv
            vertices += [nx, ny, nz]

    # indices
    #  k1--k1+1
    #  |  / |
    #  | /  |
    #  k2--k2+1
    indices = []
    lineIndices = []
    #unsigned int k1, k2
    for i in range(stackCount):
        k1 = i * (sectorCount + 1)     # beginning of current stack
        k2 = k1 + sectorCount + 1      # beginning of next stack

        #(int j = 0 j < sectorCount ++j, ++k1, ++k2)
        for j in range(sectorCount):
            # 2 triangles per sector excluding 1st and last stacks
            if(i != 0):
                indices.append(k1)
                indices.append(k2)
                indices.append(k1+1)   # k1---k2---k1+1

            if(i != (stackCount-1)):
                indices.append(k1+1)
                indices.append(k2)
                indices.append(k2+1)   # k1+1---k2---k2+1

            # vertical lines for all stacks
            lineIndices.append(k1)
            lineIndices.append(k2)
            if(i != 0):
                lineIndices.append(k1)
                lineIndices.append(k1 + 1)
            k1+=1
            k2+=1

    #print("largo ver: "+ str(len(vertices)))
    #print("largo ind: "+ str(len(indices)))
    #print("vert texCoord: "+ str(texCoord))

    return Shape(vertices, indices, image_filename)
        
    

def createColorQuad(r, g, b):

    # Defining locations and colors for each vertex of the shape    
    vertices = [
    #   positions        colors
        -0.5, -0.5, 0.0,  r, g, b,
         0.5, -0.5, 0.0,  r, g, b,
         0.5,  0.5, 0.0,  r, g, b,
        -0.5,  0.5, 0.0,  r, g, b]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
         0, 1, 2,
         2, 3, 0]

    return Shape(vertices, indices)


def createTextureQuad(image_filename, nx=1, ny=1):

    # Defining locations and texture coordinates for each vertex of the shape    
    vertices = [
    #   positions        texture
        -0.5, -0.5, 0.0,  0, 0,
         0.5, -0.5, 0.0, nx, 0,
         0.5,  0.5, 0.0, nx, ny,
        -0.5,  0.5, 0.0,  0, ny]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
         0, 1, 2,
         2, 3, 0]

    textureFileName = image_filename

    return Shape(vertices, indices, textureFileName)


def createRainbowCube():

    # Defining the location and colors of each vertex  of the shape
    vertices = [
    #    positions         colors
        -0.5, -0.5,  0.5,  1.0, 0.0, 0.0,
         0.5, -0.5,  0.5,  0.0, 1.0, 0.0,
         0.5,  0.5,  0.5,  0.0, 0.0, 1.0,
        -0.5,  0.5,  0.5,  1.0, 1.0, 1.0,
 
        -0.5, -0.5, -0.5,  1.0, 1.0, 0.0,
         0.5, -0.5, -0.5,  0.0, 1.0, 1.0,
         0.5,  0.5, -0.5,  1.0, 0.0, 1.0,
        -0.5,  0.5, -0.5,  1.0, 1.0, 1.0]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
         0, 1, 2, 2, 3, 0,
         4, 5, 6, 6, 7, 4,
         4, 5, 1, 1, 0, 4,
         6, 7, 3, 3, 2, 6,
         5, 6, 2, 2, 1, 5,
         7, 4, 0, 0, 3, 7]

    return Shape(vertices, indices)


def createColorCube(r, g, b):

    # Defining the location and colors of each vertex  of the shape
    vertices = [
    #    positions        colors
        -0.5, -0.5,  0.5, r, g, b,
         0.5, -0.5,  0.5, r, g, b,
         0.5,  0.5,  0.5, r, g, b,
        -0.5,  0.5,  0.5, r, g, b,

        -0.5, -0.5, -0.5, r, g, b,
         0.5, -0.5, -0.5, r, g, b,
         0.5,  0.5, -0.5, r, g, b,
        -0.5,  0.5, -0.5, r, g, b]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
         0, 1, 2, 2, 3, 0,
         4, 5, 6, 6, 7, 4,
         4, 5, 1, 1, 0, 4,
         6, 7, 3, 3, 2, 6,
         5, 6, 2, 2, 1, 5,
         7, 4, 0, 0, 3, 7]

    return Shape(vertices, indices)

def createColorTriangularPrism(r, g, b):

    # Defining the location and colors of each vertex  of the shape
    vertices = [
    #    positions        colors
         0.5, -0.5, -0.5, r, g, b,
         0.5,  0.5, -0.5, r, g, b,
        -0.5,  0.5,  0.5, r, g, b,
        -0.5, -0.5,  0.5, r, g, b,

        -0.5,  0.5, -0.5, r, g, b,
        -0.5, -0.5, -0.5, r, g, b,]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
         0, 1, 2, 2, 3, 0,
         1, 4, 2,
         1, 4, 5, 4, 5, 0,
         0, 5, 3,
         4, 2, 3, 2, 3, 5]

    return Shape(vertices, indices)


def createTextureCube(image_filename):

    # Defining locations and texture coordinates for each vertex of the shape  
    vertices = [
    #   positions         texture coordinates
    # Z+
        -0.5, -0.5,  0.5, 0, 0,
         0.5, -0.5,  0.5, 1, 0,
         0.5,  0.5,  0.5, 1, 1,
        -0.5,  0.5,  0.5, 0, 1,

    # Z-
        -0.5, -0.5, -0.5, 0, 0,
         0.5, -0.5, -0.5, 1, 0,
         0.5,  0.5, -0.5, 1, 1,
        -0.5,  0.5, -0.5, 0, 1,
        
    # X+
         0.5, -0.5, -0.5, 0, 0,
         0.5,  0.5, -0.5, 1, 0,
         0.5,  0.5,  0.5, 1, 1,
         0.5, -0.5,  0.5, 0, 1
,
 
    # X-
        -0.5, -0.5, -0.5, 0, 0,
        -0.5,  0.5, -0.5, 1, 0,
        -0.5,  0.5,  0.5, 1, 1,
        -0.5, -0.5,  0.5, 0, 1,

    # Y+
        -0.5,  0.5, -0.5, 0, 0,
         0.5,  0.5, -0.5, 1, 0,
         0.5,  0.5,  0.5, 1, 1,
        -0.5,  0.5,  0.5, 0, 1,

    # Y-
        -0.5, -0.5, -0.5, 0, 0,
         0.5, -0.5, -0.5, 1, 0,
         0.5, -0.5,  0.5, 1, 1,
        -0.5, -0.5,  0.5, 0, 1
        ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
          0, 1, 2, 2, 3, 0, # Z+
          7, 6, 5, 5, 4, 7, # Z-
          8, 9,10,10,11, 8, # X+
         15,14,13,13,12,15, # X-
         19,18,17,17,16,19, # Y+
         20,21,22,22,23,20] # Y-


    return Shape(vertices, indices, image_filename)


def createRainbowNormalsCube():

    sq3 = 0.57735027

    # Defining the location and colors of each vertex  of the shape
    vertices = [
            -0.5, -0.5,  0.5, 1.0, 0.0, 0.0, -sq3, -sq3, sq3,
             0.5, -0.5,  0.5, 0.0, 1.0, 0.0,  sq3, -sq3,  sq3,
             0.5,  0.5,  0.5, 0.0, 0.0, 1.0,  sq3,  sq3,  sq3,
            -0.5,  0.5,  0.5, 1.0, 1.0, 1.0, -sq3,  sq3,  sq3,

            -0.5, -0.5, -0.5, 1.0, 1.0, 0.0, -sq3, -sq3, -sq3,
             0.5, -0.5, -0.5, 0.0, 1.0, 1.0,  sq3, -sq3, -sq3,
             0.5,  0.5, -0.5, 1.0, 0.0, 1.0,  sq3,  sq3, -sq3,
            -0.5,  0.5, -0.5, 1.0, 1.0, 1.0, -sq3,  sq3, -sq3]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [0, 1, 2, 2, 3, 0,
               4, 5, 6, 6, 7, 4,
               4, 5, 1, 1, 0, 4,
               6, 7, 3, 3, 2, 6,
               5, 6, 2, 2, 1, 5,
               7, 4, 0, 0, 3, 7]

    return Shape(vertices, indices)

def createColorNormalTriangularPrism(r, g, b):

    r2 = 1 / np.sqrt(2)
    r10 = 10 / np.sqrt(10)
    # Defining the location and colors of each vertex  of the shape
    vertices = [
    #    positions        colors    normals
    # XZ+
         0.5, -0.5, -0.5, r, g, b, r2, 0, r2, # 0
         0.5,  0.5, -0.5, r, g, b, r2, 0, r2, # 1
        -0.5,  0.5,  0.5, r, g, b, r2, 0, r2, # 2
        -0.5, -0.5,  0.5, r, g, b, r2, 0, r2, # 3

    # Y+
        0.5, 0.5, -0.5, r, g, b, 0, 1, 0, # 4
        -0.5, 0.5, -0.5, r, g, b, 0, 1, 0,# 5
        -0.5, 0.5, 0.5, r, g, b, 0, 1, 0,# 6

    # Y-
        0.5, -0.5, -0.5, r, g, b, 0, -1, 0, # 7
        -0.5, -0.5, -0.5, r, g, b, 0, -1, 0, # 8
        -0.5, -0.5, 0.5, r, g, b, 0, -1, 0, # 9

    # X-
        -0.5, 0.5, -0.5, r, g, b, -1, 0, 0, # 10
        -0.5, 0.5, 0.5, r, g, b, -1, 0, 0, # 11
        -0.5, -0.5, 0.5, r, g, b, -1, 0, 0, # 12
        -0.5, -0.5, -0.5, r, g, b, -1, 0, 0, # 13

    # Z-
        0.5, 0.5, -0.5, r, g, b, 0, 0, -1, # 14
        -0.5, 0.5, -0.5, r, g, b, 0, 0, -1, # 15
        -0.5, -0.5, -0.5, r, g, b, 0, 0, -1, # 16
        0.5, -0.5, -0.5, r, g, b, 0, 0, -1, # 17
        ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
         0, 1, 2, 2, 3, 0,
         4, 5, 6,
         7, 8, 9,
         10, 11, 12, 12, 13, 10,
         14, 15, 16, 16, 17, 14]

    return Shape(vertices, indices)

def createColorNormalsCube(r, g, b):

    # Defining the location and colors of each vertex  of the shape
    vertices = [
    #   positions         colors   normals
    # Z+
        -0.5, -0.5,  0.5, r, g, b, 0,0,1,
         0.5, -0.5,  0.5, r, g, b, 0,0,1,
         0.5,  0.5,  0.5, r, g, b, 0,0,1,
        -0.5,  0.5,  0.5, r, g, b, 0,0,1,

    # Z-
        -0.5, -0.5, -0.5, r, g, b, 0,0,-1,
         0.5, -0.5, -0.5, r, g, b, 0,0,-1,
         0.5,  0.5, -0.5, r, g, b, 0,0,-1,
        -0.5,  0.5, -0.5, r, g, b, 0,0,-1,
        
    # X+
        0.5, -0.5, -0.5, r, g, b, 1,0,0,
        0.5,  0.5, -0.5, r, g, b, 1,0,0,
        0.5,  0.5,  0.5, r, g, b, 1,0,0,
        0.5, -0.5,  0.5, r, g, b, 1,0,0,
 
    # X-
        -0.5, -0.5, -0.5, r, g, b, -1,0,0,
        -0.5,  0.5, -0.5, r, g, b, -1,0,0,
        -0.5,  0.5,  0.5, r, g, b, -1,0,0,
        -0.5, -0.5,  0.5, r, g, b, -1,0,0,

    # Y+
        -0.5, 0.5, -0.5, r, g, b, 0,1,0,
         0.5, 0.5, -0.5, r, g, b, 0,1,0,
         0.5, 0.5,  0.5, r, g, b, 0,1,0,
        -0.5, 0.5,  0.5, r, g, b, 0,1,0,

    # Y-
        -0.5, -0.5, -0.5, r, g, b, 0,-1,0,
         0.5, -0.5, -0.5, r, g, b, 0,-1,0,
         0.5, -0.5,  0.5, r, g, b, 0,-1,0,
        -0.5, -0.5,  0.5, r, g, b, 0,-1,0
        ]

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
          0, 1, 2, 2, 3, 0, # Z+
          7, 6, 5, 5, 4, 7, # Z-
          8, 9,10,10,11, 8, # X+
         15,14,13,13,12,15, # X-
         19,18,17,17,16,19, # Y+
         20,21,22,22,23,20] # Y-

    return Shape(vertices, indices)


def createTextureNormalsCube(image_filename):

    # Defining locations,texture coordinates and normals for each vertex of the shape  
    vertices = [
    #   positions            tex coords   normals
    # Z+
        -0.5, -0.5,  0.5,    0, 0,        0,0,1,
         0.5, -0.5,  0.5,    1, 0,        0,0,1,
         0.5,  0.5,  0.5,    1, 1,        0,0,1,
        -0.5,  0.5,  0.5,    0, 1,        0,0,1,   
    # Z-          
        -0.5, -0.5, -0.5,    0, 0,        0,0,-1,
         0.5, -0.5, -0.5,    1, 0,        0,0,-1,
         0.5,  0.5, -0.5,    1, 1,        0,0,-1,
        -0.5,  0.5, -0.5,    0, 1,        0,0,-1,
       
    # X+          
         0.5, -0.5, -0.5,    0, 0,        1,0,0,
         0.5,  0.5, -0.5,    1, 0,        1,0,0,
         0.5,  0.5,  0.5,    1, 1,        1,0,0,
         0.5, -0.5,  0.5,    0, 1,        1,0,0,   
    # X-          
        -0.5, -0.5, -0.5,    0, 0,        -1,0,0,
        -0.5,  0.5, -0.5,    1, 0,        -1,0,0,
        -0.5,  0.5,  0.5,    1, 1,        -1,0,0,
        -0.5, -0.5,  0.5,    0, 1,        -1,0,0,   
    # Y+          
        -0.5,  0.5, -0.5,    0, 0,        0,1,0,
         0.5,  0.5, -0.5,    1, 0,        0,1,0,
         0.5,  0.5,  0.5,    1, 1,        0,1,0,
        -0.5,  0.5,  0.5,    0, 1,        0,1,0,   
    # Y-          
        -0.5, -0.5, -0.5,    0, 0,        0,-1,0,
         0.5, -0.5, -0.5,    1, 0,        0,-1,0,
         0.5, -0.5,  0.5,    1, 1,        0,-1,0,
        -0.5, -0.5,  0.5,    0, 1,        0,-1,0
        ]   

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = [
          0, 1, 2, 2, 3, 0, # Z+
          7, 6, 5, 5, 4, 7, # Z-
          8, 9,10,10,11, 8, # X+
         15,14,13,13,12,15, # X-
         19,18,17,17,16,19, # Y+
         20,21,22,22,23,20] # Y-

    return Shape(vertices, indices, image_filename)