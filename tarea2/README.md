# Documentación
El informe del programa junto con las instrucciones de  uso están en el archivo `Informe_tarea2`

# Programa
El programa general está compactado en el archivo `init.py` el cual ocupa las librerias correspondientes a OpenGL para mostrar objetos en 3D.

Los archivos complementarios e indispensables para el funcionamiento son `transformations2.py`, `basic_shapes.py`, `scene_graph2.py`, `easy_shaders.py`, como también los directorios aux6_v2, aux4 y image