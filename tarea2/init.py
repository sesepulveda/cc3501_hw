
# coding=utf-8
"""
Sebastian Sepulveda

IMPORTANTE: No se siguió todas las instrucciones debido a la limitación que implicaba seguir algunas y se prefirió explorar otros medios.
Se eligió la elección del Mouse pero se trabaja con las teclas para mejor visualización

INSTRUCCIONES:

Con el teclado, se mueve la camara:
KEY_W: Realiza un acercamiento hacia la imagen
KEY_S: Realiza un alejamiento de la imagen
KEY_A: Mueve la imagen hacia la derecha
KEY_D: Mueve la imagen hacia la izquierda

Con el mouse:
Se modifica el movimiento del "ojo" horizontal y vertical
Con el scroll del mouse:
Se realiza una modificación de la perspectiva, realizando el efecto del zoom.

La visualización empieza con la vista en perspectiva, si se cambia a otra vista no funcionan todas las configuraciones realizadas.
Se modifica el archivo ex_projections.py
Se ocupan nuevos archivos proporcionados en clases

"""

import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import sys
import math

#from ex_obj_reader import *
from aux4.ex_aux_4 import *
from aux4.ex_curves import *
import aux6_v2.lib.basic_shapes_extended as bs_ext
import aux6_v2.lib.catrom as catrom

import transformations2 as tr2
import basic_shapes as bs
import scene_graph2 as sg
import easy_shaders as es


PROJECTION_ORTHOGRAPHIC = 0
PROJECTION_FRUSTUM = 1
PROJECTION_PERSPECTIVE = 2



# A class to store the application control
class Controller:
    def __init__(self):
        self.fillPolygon = True
        self.showAxis = False
        self.follow_car = False
        self.lights = False
        self.ChangeView = False
        self.projection = PROJECTION_PERSPECTIVE
        self.POSCAM = 0
        self.estatica = False
        self.show = False
        self.changeGround = False

    firstMouse = True
    cameraPos   = np.array([4.91995052, 4.9419168, 2.48825306])
    cameraFront = np.array([-0.71040869, -0.70300799, -0.03315518])
    cameraUp    = np.array([0.0, 0.0,  1.0])
    yaw   = -135.28   # yaw is initialized to -90.0 degrees since a 
                            # yaw of 0.0 results in a direction 
                            # vector pointing to the right so we initially rotate a bit to the left.
    pitch =  -1.9
    lastX =  1649.0
    lastY =  195.0
    fov   =  4000.0
    move_sides = 0.0
    move_updown = 0.0

    #def inicio(self, cameraPos, cameraFront, cameraUp, yaw, pitch, fov,lastX,lastY):
    #    if self.estatica:
            
    '''
    ([4.91995052, 4.9419168, 2.48825306])
    ([-0.71040869, -0.70300799, -0.03315518])
    eye = np.array([10.04164632, 13.99480324, 14.19807597])
        at = np.array([-0.03541168, -0.69186712, -0.72115594])

    eye = np.array([4.91995052 4.9419168  2.48825306]
    at = np.array([-0.71040869 -0.70300799 -0.03315518]
    up = np.array([0. 0. 1.]
    controller.lastX = 1649.0
    controller.lastY = 195.0
    controller.pitch = -1.9000000000000004
    controller.fov = 5000.0
    controller.yaw = -135.29999999999976

        camPos = [-33.37429533  57.88633694  11.79648508]
        camFront = [ 0.50306245 -0.85573735 -0.12100314]
        camUp = [0. 0. 1.]
        lastX = -642.0
        lastY = 271.0
        pitch = -6.9499999999999815
        fov = 842.9166666666661
        yaw = -59.550000000000665

        cameraPos   = np.array([0.0, 80.0,  0.0])
        cameraFront = np.array([0.0, -1.0, 0.0])
        cameraUp    = np.array([0.0, 0.0,  1.0])
        yaw   = -90.0   # yaw is initialized to -90.0 degrees since a 
                                # yaw of 0.0 results in a direction 
                                # vector pointing to the right so we initially rotate a bit to the left.
        pitch =  0.0
        lastX =  600.0 / 2.0
        lastY =  600.0 / 2.0
        fov   =  450.0
        move_sides = 0.0
        move_updown = 0.0
    '''

# We will use the global controller as communication with the callback function
controller = Controller()


'''#####################################################

    REF: https://learnopengl.com/Getting-started/Camera

    Movimiento con el Mouse - Look around - Euler angles
    
    Los Angulos de euler se separan en 3 tipos pitch, yaw and roll.
    En esta configuracion solo se modifican el pitch y el yaw, y se utilizarán
    los cursores del teclado (A,W,S,D) para mover la camara con más libertad

    Mouse callback: se reciben las posiciones del cursor del mouse las cuales modificarán
    a los angulos yaw y pitch

    Scroll callback: realiza un zoom distinto a lo que se reealiza con las teclas,
    pues no realiza un movimiento de la camara, si no que hace un cambio en la 
    vista en perspectiva  
'''#####################################################


def mouse_callback(window, xpos, ypos):
    global controller
    if (controller.firstMouse):
        controller.lastX = xpos
        controller.lastY = ypos
        controller.firstMouse = False
    '''
    print('primerx: ' + str(controller.lastX))
    print('primery: ' + str(controller.lastY))
    '''

    xoffset = -xpos + controller.lastX # reversed since x-coordinates go from bottom to top
    yoffset = controller.lastY - ypos # reversed since y-coordinates go from bottom to top
    controller.lastX = xpos
    controller.lastY = ypos

    sensitivity = 0.05 # change this value to your liking
    xoffset *= sensitivity
    yoffset *= sensitivity

    controller.yaw += xoffset
    controller.pitch += yoffset

    #make sure that when controller.pitch is out of bounds, screen doesn't get flipped
    if (controller.pitch > 89.0):
        controller.pitch = 89.0
    if (controller.pitch < -89.0):
        controller.pitch = -89.0

    ex = np.cos(math.radians(controller.yaw)) * np.cos(math.radians(controller.pitch))
    ey = np.sin(math.radians(controller.pitch))
    ez = np.sin(math.radians(controller.yaw)) * np.cos(math.radians(controller.pitch))

    ex = np.cos(math.radians(controller.yaw)) * np.cos(math.radians(controller.pitch))
    ey = np.sin(math.radians(controller.yaw)) * np.cos(math.radians(controller.pitch))
    ez = np.sin(math.radians(controller.pitch))
    front=np.array([ex,ey,ez])
    #print(np.cos(math.radians(controller.yaw)) * np.cos(math.radians(controller.pitch)))
    controller.cameraFront = front / np.linalg.norm(front)

def scroll_callback(window, xoffset, yoffset):
    #print(yoffset)
    if (controller.fov >= -20.0 and controller.fov <= 10000.0):
        controller.fov -= yoffset*10
        #print(controller.fov)
    if (controller.fov < -20.0):
        controller.fov = -20.0
    if (controller.fov > 10000.0):
        controller.fov = 10000.0

def changeView(n):
    
    if n==1:
        eye = np.array([ 0.14544578 ,-8.7404129 ,  4.91253141])
        at = np.array([ 0.01903554 , 0.96505721, -0.26134694])
        up = np.array([0., 0., 1.])
        normal_view = tr2.lookAt(eye,at,up)
    elif n==2:
        eye = np.array([ 9.89438631, -0.11091709 , 2.61833856])
        at = np.array([-0.99299852 , 0.0117857 ,  0.1175374 ])
        up = np.array([0., 0., 1.])
        normal_view = tr2.lookAt(eye,at,up)

    elif n==3:
        eye = np.array([-0.29059818 , 9.24681088 , 5.07846319])
        at = np.array([-0.00134932 ,-0.96637514 ,-0.25713279])
        up = np.array([0.0, 0.0,  1.0])
        normal_view = tr2.lookAt(eye,at,up)
    elif n==4:  
        eye = np.array([-8.4970347 , -0.03211853 , 5.85733488])
        at = np.array([ 0.9494213 ,  0.002817   ,-0.31399246])
        up = np.array([0.0, 0.0,  1.0])
        normal_view = tr2.lookAt(eye,at,up)

    elif n==5:  
        eye = np.array([10.04164632, 13.99480324, 14.19807597])
        at = np.array([-0.03541168, -0.69186712, -0.72115594])
        up = np.array([0., 0., 1.])
        normal_view = tr2.lookAt(eye,at,up)

    return normal_view

def on_key(window, key, scancode, action, mods):

    if action != glfw.PRESS:
        return
    
    global controller

    if key == glfw.KEY_SPACE:
        controller.fillPolygon = not controller.fillPolygon

    #elif key == glfw.KEY_7:
        #print('Orthographic projection')
        #controller.projection = PROJECTION_ORTHOGRAPHIC

    #elif key == glfw.KEY_8:
        #print('Frustum projection')
        #controller.projection = PROJECTION_FRUSTUM

    elif key == glfw.KEY_9:
        print('Perspective projection')
        controller.projection = PROJECTION_PERSPECTIVE

    elif key == glfw.KEY_RIGHT_CONTROL:
        print('reset ubication')
        controller.ChangeView = False

    elif key == glfw.KEY_G:
        print('Ground Changed')
        controller.changeGround = not controller.changeGround

    elif key == glfw.KEY_1:
        print('Camera 1 Localized')
        controller.POSCAM = 1
        controller.ChangeView = True
        

    elif key == glfw.KEY_2:
        print('Camera 2 Localized')
        controller.POSCAM = 2
        controller.ChangeView = True
        

    elif key == glfw.KEY_3:
        print('Camera 3 Localized')
        controller.POSCAM = 3
        controller.ChangeView = True
        

    elif key == glfw.KEY_4:
        print('Camera 4 Localized')
        controller.POSCAM = 4
        controller.ChangeView = True

    elif key == glfw.KEY_O:
        print('IM YOUR FATHER')
        controller.show = not controller.show
        

    elif key == glfw.KEY_LEFT_CONTROL:
        controller.showAxis = not controller.showAxis

    elif key == glfw.KEY_ESCAPE:
        sys.exit()


def on_key_with_arrows(window, key, scancode, action, mods):
    if action != glfw.PRESS:
        return

    global controller

    if key == glfw.KEY_SPACE:
        controller.fillPolygon = not controller.fillPolygon

    elif key == glfw.KEY_1:
        print('Orthographic projection')
        controller.projection = PROJECTION_ORTHOGRAPHIC

    elif key == glfw.KEY_2:
        print('Frustum projection')
        controller.projection = PROJECTION_FRUSTUM

    elif key == glfw.KEY_3:
        print('Perspective projection')
        controller.projection = PROJECTION_PERSPECTIVE

    elif key == glfw.KEY_RIGHT:
        print('KEY_RIGHT')

    elif key == glfw.KEY_LEFT:
        print('KEY_RIGHT')

    elif key == glfw.KEY_UP:
        print('KEY_RIGHT')

    elif key == glfw.KEY_DOWN:
        print('KEY_RIGHT')

    elif key == glfw.KEY_ESCAPE:
        sys.exit()

def createScene(file):
    gpuTextureCube = es.toGPUShape(bs.createTextureCube(file), GL_REPEAT, GL_NEAREST)

    cielo_scaled = sg.SceneGraphNode("cielo_scaled")
    cielo_scaled.transform = tr2.scale(50, 50, 30)
    cielo_scaled.childs += [gpuTextureCube]

    cielo_rotated = sg.SceneGraphNode("cielo_rotated_x")
    cielo_rotated.transform = tr2.rotationX(0)
    cielo_rotated.childs += [cielo_scaled]

    cielo = sg.SceneGraphNode("cielo")
    cielo.transform = tr2.translate(0, 0, 14.)
    cielo.childs += [cielo_rotated]

    return cielo

def createCone():
    c = [1.0,1.0,1.0]
    gpuCone = es.toGPUShape(bs.createCone(c[0],c[1],c[2]))


    cone_scaled = sg.SceneGraphNode("cone_scaled")
    cone_scaled.transform = tr2.scale(1, 1, 1)
    cone_scaled.childs += [gpuCone]

    cone_rotated = sg.SceneGraphNode("cone_rotated_x")
    cone_rotated.transform = tr2.rotationX(0)
    cone_rotated.childs += [cone_scaled]

    cone = sg.SceneGraphNode("cone")
    cone.transform = tr2.translate(0, 0, 10)
    cone.childs += [cone_rotated]
    return cone

def createCircle():
    gpuCircle = es.toGPUShape(bs.createCircle([1,0,0],1,20))
    circle_scaled = sg.SceneGraphNode("circle_scaled")
    circle_scaled.transform = tr2.scale(1, 1, 1)
    circle_scaled.childs += [gpuCircle]

    circle_rotated = sg.SceneGraphNode("circle_rotated_x")
    circle_rotated.transform = tr2.rotationX(0)
    circle_rotated.childs += [circle_scaled]

    circle = sg.SceneGraphNode("circle")
    circle.transform = tr2.translate(10, 10, 5)
    circle.childs += [circle_rotated]
    return circle

def createCircleTexture():
    gpuCircle = es.toGPUShape(bs.createTextureCircle("image/towe_black.jpg"), GL_REPEAT, GL_NEAREST)
    circle_scaled = sg.SceneGraphNode("circle_scaled")
    circle_scaled.transform = tr2.scale(1, 1, 1)
    circle_scaled.childs += [gpuCircle]

    circle_rotated = sg.SceneGraphNode("circle_rotated_x")
    circle_rotated.transform = tr2.rotationX(0)
    circle_rotated.childs += [circle_scaled]

    circle = sg.SceneGraphNode("circle")
    circle.transform = tr2.translate(10, 10, 5)
    circle.childs += [circle_rotated]
    return circle


def createCylinder():
    color = [1.0,0.,0.]
    gpuPrisma = es.toGPUShape(bs.createCylinder(color[0],color[1],color[2],30))

    prisma_scaled = sg.SceneGraphNode("prisma_scaled")
    prisma_scaled.transform = tr2.scale(1, 1, 1)
    prisma_scaled.childs += [gpuPrisma]

    prisma_rotated = sg.SceneGraphNode("prisma_rotated_x")
    prisma_rotated.transform = tr2.rotationX(0)
    prisma_rotated.childs += [prisma_scaled]

    prisma = sg.SceneGraphNode("prisma")
    prisma.transform = tr2.translate(0, 0, 10)
    prisma.childs += [prisma_rotated]
    return prisma

#createSphere(radius,stackCount,sectorCount)
def createEsfera():
    gpuPrisma = es.toGPUShape(bs.createSphereTexture("image/death_start.jpg",1), GL_REPEAT, GL_NEAREST)

    prisma_scaled = sg.SceneGraphNode("prisma_scaled")
    prisma_scaled.transform = tr2.scale(1, 1, 1)
    prisma_scaled.childs += [gpuPrisma]

    prisma_rotated = sg.SceneGraphNode("prisma_rotated_x")
    prisma_rotated.transform = tr2.rotationX(0)
    prisma_rotated.childs += [prisma_scaled]

    prisma = sg.SceneGraphNode("prisma")
    prisma.transform = tr2.translate(0, -10, 10)
    prisma.childs += [prisma_rotated]
    return prisma


def createCube(n):
    #gpuTextureCube = es.toGPUShape(bs.createTextureCube("tower_noir_windows.jpg"), GL_REPEAT, GL_NEAREST)
    gpuTextureCube = es.toGPUShape(bs.createTextureCube("image/towe_black.jpg"), GL_REPEAT, GL_NEAREST)
    gpuTextureWin = es.toGPUShape(bs.createTextureCube("image/towe_black.jpg"), GL_REPEAT, GL_NEAREST)

    #--------------------
    #tower base
    cube_scaled = sg.SceneGraphNode("cube_scaled")
    cube_scaled.transform = tr2.scale(.7, .7, 1)
    cube_scaled.childs += [gpuTextureCube]

    cube_rotated = sg.SceneGraphNode("cube_rotated_x")
    cube_rotated.transform = tr2.rotationX(0)
    cube_rotated.childs += [cube_scaled]

    cube_tower = sg.SceneGraphNode("cube_tower")
    cube_tower.transform = tr2.translate(0, 0, .5)
    cube_tower.childs += [cube_rotated]

    #--------------------
    #tower with windows
    pisos = []
    for i in range(n):
        nom_scale = "win_scaled_" + str(i)
        win_scaled = sg.SceneGraphNode(nom_scale)
        win_scaled.transform = tr2.scale(.7, .7, .25)
        win_scaled.childs += [gpuTextureWin]

        nom_rotated = "win_rotated_" + str(i)
        win_rotated = sg.SceneGraphNode(nom_rotated)
        win_rotated.transform = tr2.rotationX(0)
        win_rotated.childs += [win_scaled]

        nom_trans = "win_trans_" + str(i)
        win_trans = sg.SceneGraphNode(nom_trans)
        win_trans.transform = tr2.translate(0,0,1.1 + i*0.25)
        win_trans.childs += [win_scaled]
        pisos.append(win_trans)

    cube = sg.SceneGraphNode("cube")
    cube.transform = tr2.translate(0,0,0)
    cube.childs += [cube_tower]
    for i in range(len(pisos)):
        cube.childs += [pisos[i]]
    return cube

def arbol():
    c = [8/255.0, 162/255.0, 29/255.0]
    ct = [162/255.0, 111/255.0, 8/255.0]
    gpuCone = es.toGPUShape(bs.createCone(c[0],c[1],c[2]))
    hojas = sg.SceneGraphNode("verde")
    hojas.transform = tr2.scale(.2, .2, .3)
    hojas.childs += [gpuCone]

    hojas_traslate = sg.SceneGraphNode("hojas_traslate")
    hojas_traslate.transform = tr2.translate(0., 0., .3)
    hojas_traslate.childs += [hojas]

    gpuCylinder = es.toGPUShape(bs.createCylinder(ct[0], ct[1], ct[2], 30 ))
    tronco = sg.SceneGraphNode("cafe")
    tronco.transform = tr2.scale(.1, .1, .1)
    tronco.childs += [gpuCylinder]

    ub = [
    [-2., -2.8, .0],
    [-2., 1.7, .0],
    [2.7, -1.2, .0],
    [2.7, 2.8, .0],
    [3.2, -3.2, .0],
    [.7, .7, 3.5],
    [-.7, .0, 4.7]
    ]

    arboleda=[]

    for i in range(5):
        nom = "arbol_" + str(i)
        arbol = sg.SceneGraphNode(nom)
        arbol.transform = tr2.translate(ub[i][0], ub[i][1], ub[i][2])
        arbol.childs += [hojas_traslate]
        arbol.childs += [tronco]
        arboleda.append(arbol)

    arboleda_node = sg.SceneGraphNode("arboleda")
    for i in range(len(arboleda)):
        arboleda_node.childs += [arboleda[i]]

    return arboleda_node

def antena():
    c = [160/255.0, 160/255.0, 160/255.0]
    ct = [58/255.0, 85/255.0, 89/255.0]
    gpuCube = es.toGPUShape(bs.createColorCube(ct[0], ct[1], ct[2]))
    gpuSphere = es.toGPUShape(bs.createSphere(0.8,30,30,c))
    gpuCone = es.toGPUShape(bs.createCylinder(c[0], c[1], c[2], 50))
    gpuCone_1 = es.toGPUShape(bs.createCylinder(c[0], c[1], c[2], 50))

    pilar = sg.SceneGraphNode("pilar")
    pilar.transform = tr2.scale(.05, .05, .06)
    pilar.childs += [gpuCone]

    pilar_1 = sg.SceneGraphNode("pilar_1")
    pilar_1.transform = tr2.scale(.03, .03, .06)
    pilar_1.childs += [gpuCone_1]

    pilar_translate = sg.SceneGraphNode("pilar_translate")
    pilar_translate.transform = tr2.translate(0., 0., 5.7)
    pilar_translate.childs += [pilar]

    pilar_1_translate = sg.SceneGraphNode("pilar_1_translate")
    pilar_1_translate.transform = tr2.translate(0., 0., 5.9)
    pilar_1_translate.childs += [pilar_1]

    pilar_2 = sg.SceneGraphNode("pilar_2")
    pilar_2.transform = tr2.scale(.01, .01, .08)
    pilar_2.childs += [gpuCone_1]

    pilar_2_translate = sg.SceneGraphNode("pilar_2_translate")
    pilar_2_translate.transform = tr2.translate(0., 0., 6.)
    pilar_2_translate.childs += [pilar_2]

    esfera = sg.SceneGraphNode("esfera")
    esfera.transform = tr2.scale(.06, .06, .06)
    esfera.childs += [gpuSphere]

    esfera_translate = sg.SceneGraphNode("esfera_translate")
    esfera_translate.transform = tr2.translate(.0, .0, 6.6)
    esfera_translate.childs += [esfera]

    cubo = sg.SceneGraphNode("cubo")
    cubo.transform = tr2.scale(.6, .4, .08)
    cubo.childs += [gpuCube]

    cubo_translate = sg.SceneGraphNode("cubo_translate")
    cubo_translate.transform = tr2.translate(.3, .0, 5.75)
    cubo_translate.childs += [cubo]

    antena_1 = sg.SceneGraphNode("antena_1")
    antena_1.transform = tr2.translate(.9, -0.2, 0.)
    antena_1.childs += [pilar_2_translate]
    antena_1.childs += [pilar_1_translate]
    antena_1.childs += [pilar_translate]
    #antena_1.childs += [esfera_translate]

    antena_2 = sg.SceneGraphNode("antena_2")
    antena_2.transform = tr2.translate(-0.1, 0., 0.)
    antena_2.childs += [pilar_2_translate]
    antena_2.childs += [pilar_1_translate]
    antena_2.childs += [pilar_translate]
    antena_2.childs += [esfera_translate]

    #antena_2 = sg.SceneGraphNode("antena_2")
    #antena_2.transform = tr2.translate(-0.1, 0., 0.)

    antena_3 = sg.SceneGraphNode("antena_3")
    antena_3.transform = tr2.translate(.9, .2, 0.)
    antena_3.childs += [pilar_2_translate]
    antena_3.childs += [pilar_1_translate]
    antena_3.childs += [pilar_translate]
    #antena_3.childs += [esfera_translate]

    antena = sg.SceneGraphNode("antena")
    antena.childs += [antena_1]
    antena.childs += [antena_2]
    antena.childs += [antena_3]
    antena.childs += [cubo_translate]
    return antena

def plataformas():
    ct = [91/255.0, 50/255.0, 41/255.0]
    gpuCube = es.toGPUShape(bs.createColorCube(ct[0], ct[1], ct[2]))

    sc = [
    [.35, .35, .2],
    [.35, .35, .2],
    [.35, .35, .2],
    [.35, .35, .2],
    [.35, .35, .2],
    [.35, .35, .2],
    [.35, .35, .2]
    ]

    ub = [
    [-.02, .8, 4.7],
    [-.7, .7, 3.],
    [-.7, -.7, 3.5],
    [-.02, -.8, 4.7],
    [.7, -.7, 3.],
    [.7, .7, 3.5],
    [-.7, .0, 4.7]
    ]

    superPlataforms = []
    for i in range(7):
        nom = "cubo_" + str(i)
        cubo_0 = sg.SceneGraphNode(nom)
        cubo_0.transform = tr2.scale(sc[i][0], sc[i][1], sc[i][2])
        cubo_0.childs += [gpuCube]

        cubo_0_translate = sg.SceneGraphNode("cubo_0_translate")
        cubo_0_translate.transform = tr2.translate(ub[i][0],ub[i][1],ub[i][2])
        cubo_0_translate.childs += [cubo_0]

        superPlataforms.append(cubo_0_translate)

    plataforma = sg.SceneGraphNode("plataformsGral")
    for i in range(len(superPlataforms)):
        plataforma.childs += [superPlataforms[i]]

    return plataforma

def createTower():
    cube = createCube(19)
    cube_tam = sg.SceneGraphNode("cube_tam1")
    cube_tam.transform = tr2.translate(0, 0, 0)
    cube_tam.childs += [cube]

    cube1 = createCube(10)
    cube_tam1 = sg.SceneGraphNode("cube_tam1")
    cube_tam1.transform = tr2.translate(.7, .7, 0)
    cube_tam1.childs += [cube1]

    cube2 = createCube(15)
    cube_tam2 = sg.SceneGraphNode("cube_tam2")
    cube_tam2.transform = tr2.translate(0, .7, 0)
    cube_tam2.childs += [cube2]

    cube3 = createCube(8)
    cube_tam3 = sg.SceneGraphNode("cube_tam3")
    cube_tam3.transform = tr2.translate(-.7, .7, 0)
    cube_tam3.childs += [cube3]

    cube4 = createCube(15)
    cube_tam4 = sg.SceneGraphNode("cube_tam4")
    cube_tam4.transform = tr2.translate(-.7, 0, 0)
    cube_tam4.childs += [cube4]

    cube5 = createCube(10)
    cube_tam5 = sg.SceneGraphNode("cube_tam5")
    cube_tam5.transform = tr2.translate(-.7, -.7, 0)
    cube_tam5.childs += [cube5]

    cube6 = createCube(15)
    cube_tam6 = sg.SceneGraphNode("cube_tam6")
    cube_tam6.transform = tr2.translate(0, -.7, 0)
    cube_tam6.childs += [cube6]

    cube7 = createCube(8)
    cube_tam7 = sg.SceneGraphNode("cube_tam7")
    cube_tam7.transform = tr2.translate(.7, -.7, 0)
    cube_tam7.childs += [cube7]

    cube8 = createCube(19)
    cube_tam8 = sg.SceneGraphNode("cube_tam8")
    cube_tam8.transform = tr2.translate(.7, 0, 0)
    cube_tam8.childs += [cube8]

    tower = sg.SceneGraphNode("tower")
    tower.childs += [cube_tam]
    tower.childs += [cube_tam1]
    tower.childs += [cube_tam2]
    tower.childs += [cube_tam3]
    tower.childs += [cube_tam4]
    tower.childs += [cube_tam5]
    tower.childs += [cube_tam6]
    tower.childs += [cube_tam7]
    tower.childs += [cube_tam8]

    return tower


def createCar(r1,g1,b1, r2, g2, b2, isNormal):
    if isNormal:
        gpuBlackQuad = es.toGPUShape(bs.createColorNormalsCube(0, 0, 0))
        gpuChasisQuad_color1 = es.toGPUShape(bs.createColorNormalsCube(r1, g1, b1))
        gpuChasisQuad_color2 = es.toGPUShape(bs.createColorNormalsCube(r2, g2, b2))
        gpuChasisPrism = es.toGPUShape(bs.createColorNormalTriangularPrism(153 / 255, 204 / 255, 255 / 255))
    else:
        gpuBlackQuad = es.toGPUShape(bs.createCylinder(0,0,0,30))
        gpuChasisQuad_color1 = es.toGPUShape(bs.createColorCube(r1,g1,b1))
        gpuChasisQuad_color2 = es.toGPUShape(bs.createColorCube(r2,g2,b2))
        gpuChasisPrism = es.toGPUShape(bs.createColorTriangularPrism(153/255, 204/255, 255/255))
    
    # Cheating a single wheel
    wheel = sg.SceneGraphNode("wheel")
    wheel.transform = tr2.scale(0.15, 0.15, 0.1)
    wheel.childs += [gpuBlackQuad]

    wheelRotation_1 = sg.SceneGraphNode("wheelRotation_1")
    wheelRotation_1.transform = tr2.rotationY(np.pi/2)
    wheelRotation_1.childs += [wheel]

    wheelRotation_2 = sg.SceneGraphNode("wheelRotation_2")
    wheelRotation_2.transform = tr2.rotationZ(np.pi/2)
    wheelRotation_2.childs += [wheelRotation_1]

    wheelRotation = sg.SceneGraphNode("wheelRotation")
    wheelRotation.transform = tr2.translate(0,-.3,0)
    wheelRotation.childs += [wheelRotation_2]

    # Instanciating 2 wheels, for the front and back parts
    frontWheel = sg.SceneGraphNode("frontWheel")
    frontWheel.transform = tr2.translate(0.3,0,-0.3)
    frontWheel.childs += [wheelRotation]

    backWheel = sg.SceneGraphNode("backWheel")
    backWheel.transform = tr2.translate(-0.3,0,-0.3)
    backWheel.childs += [wheelRotation]
    
    # Creating the bottom chasis of the car
    bot_chasis = sg.SceneGraphNode("bot_chasis")
    bot_chasis.transform = tr2.scale(1.1,0.7,0.1)
    bot_chasis.childs += [gpuChasisQuad_color1]

    # Moving bottom chasis
    moved_b_chasis = sg.SceneGraphNode("moved_b_chasis")
    moved_b_chasis.transform = tr2.translate(0, 0, -0.2)
    moved_b_chasis.childs += [bot_chasis]

    # Creating light support
    light_s = sg.SceneGraphNode("light_s")
    light_s.transform = tr2.scale(1, 0.1, 0.1)
    light_s.childs += [gpuChasisQuad_color2]

    # Creating right light
    right_light = sg.SceneGraphNode("right_light")
    right_light.transform = tr2.translate(0, 0.25, 0)
    right_light.childs += [light_s]

    # Moving right light
    left_light = sg.SceneGraphNode("left_light")
    left_light.transform = tr2.translate(0, -0.25, 0)
    left_light.childs += [light_s]

    # Creating center chasis
    center_chasis = sg.SceneGraphNode("center_chasis")
    center_chasis.transform = tr2.scale(1, 0.4, 0.15)
    center_chasis.childs += [gpuChasisQuad_color1]

    # Moving center chasis
    m_center_chasis = sg.SceneGraphNode("m_center_chasis")
    m_center_chasis.transform = tr2.translate(0.05, 0, 0)
    m_center_chasis.childs += [center_chasis]

    # Creating center quad
    center_quad = sg.SceneGraphNode("center_quad")
    center_quad.transform = tr2.scale(0.26, 0.5, 0.2)
    center_quad.childs += [gpuChasisQuad_color2]

    # Moving center quad
    m_center_quad = sg.SceneGraphNode("m_center_quad")
    m_center_quad.transform = tr2.translate(-0.07, 0, 0.1)
    m_center_quad.childs += [center_quad]

    # Creating front wind shield
    f_wind_shield = sg.SceneGraphNode("f_wind_shield")
    f_wind_shield.transform = tr2.scale(0.25, 0.5, 0.2)
    f_wind_shield.childs += [gpuChasisPrism]

    # Moving front wind shield
    m_f_wind_shield = sg.SceneGraphNode("m_f_wind_shield")
    m_f_wind_shield.transform = tr2.translate(0.2, 0, 0.1)
    m_f_wind_shield.childs += [f_wind_shield]

    # Creating back wind shield
    b_wind_shield = sg.SceneGraphNode("b_wind_shield")
    b_wind_shield.transform = tr2.scale(0.25, 0.5, 0.2)
    b_wind_shield.childs += [gpuChasisPrism]

    # Rotate back wind shield
    r_b_wind_shield = sg.SceneGraphNode("r_b_wind_shield")
    r_b_wind_shield.transform = tr2.rotationZ(np.pi)
    r_b_wind_shield.childs += [b_wind_shield]

    # Moving back wind shield
    m_b_wind_shield = sg.SceneGraphNode("m_b_wind_shield")
    m_b_wind_shield.transform = tr2.translate(-0.3, 0, 0.1)
    m_b_wind_shield.childs += [r_b_wind_shield]

    # Joining chasis parts
    complete_chasis = sg.SceneGraphNode("complete_chasis")
    complete_chasis.childs += [moved_b_chasis]
    complete_chasis.childs += [right_light]
    complete_chasis.childs += [left_light]
    complete_chasis.childs += [m_center_chasis]
    complete_chasis.childs += [m_center_quad]
    complete_chasis.childs += [m_b_wind_shield]
    complete_chasis.childs += [m_f_wind_shield]


    # All pieces together
    car = sg.SceneGraphNode("car")
    car.childs += [complete_chasis]
    car.childs += [frontWheel]
    car.childs += [backWheel]

    return car


# Create ground with textures
def createGround(file):
    gpuGround_texture = es.toGPUShape(bs.createTextureQuad(file), GL_REPEAT, GL_LINEAR)

    ground_scaled = sg.SceneGraphNode("ground_scaled")
    ground_scaled.transform = tr2.scale(1, 1, 1)
    ground_scaled.childs += [gpuGround_texture]

    ground_tras = sg.SceneGraphNode("ground_tras")
    ground_tras.transform = tr2.translate(-2.5, -2.5, 0)
    ground_tras.childs += [ground_scaled]

    lista = []

    for i in range(8):
        for j in range(8):
            nom = 'ground_' + str(i) + str(j)
            ground = sg.SceneGraphNode("ground")
            ground.transform = tr2.translate(-1 + 1*i,-1 + 1*j, 0)
            ground.childs += [ground_tras]
            lista.append(ground)
    
    suelo = sg.SceneGraphNode("suelo")
    for i in range(len(lista)):
        suelo.childs += [lista[i]]

    return suelo

def createCamino(file):
    gpuGround_texture = es.toGPUShape(bs.createTextureQuad(file), GL_REPEAT, GL_NEAREST)
    ground_scaled = sg.SceneGraphNode("ground_scaled")
    ground_scaled.transform = tr2.scale(50, 50, 10)
    ground_scaled.childs += [gpuGround_texture]

    ground_rotated = sg.SceneGraphNode("ground_rotated_x")
    ground_rotated.transform = tr2.rotationX(0)
    ground_rotated.childs += [ground_scaled]

    ground = sg.SceneGraphNode("ground")
    ground.transform = tr2.translate(0, 0, -0.5)
    ground.childs += [ground_rotated]

    gpuSky_texture = es.toGPUShape(bs.createTextureQuad("image/starts.jpg"), GL_REPEAT, GL_NEAREST)

    sky_scaled = sg.SceneGraphNode("sky_scaled")
    sky_scaled.transform = tr2.scale(50, 50, 10)
    sky_scaled.childs += [gpuSky_texture]

    sky = sg.SceneGraphNode("sky")
    sky.transform = tr2.translate(0, 0, 25.)
    sky.childs += [sky_scaled]

    escenario = sg.SceneGraphNode("escenario")
    escenario.childs += [sky]
    escenario.childs += [ground]

    return escenario

def createGroundSphere():
    gpuPrisma = es.toGPUShape(bs.createSphereTexture("image/city_2.jpg",30), GL_REPEAT, GL_NEAREST)

    prisma_scaled = sg.SceneGraphNode("prisma_scaled")
    prisma_scaled.transform = tr2.scale(1, 1, 1)
    prisma_scaled.childs += [gpuPrisma]

    prisma_rotated = sg.SceneGraphNode("prisma_rotated_x")
    prisma_rotated.transform = tr2.rotationX(0)
    prisma_rotated.childs += [prisma_scaled]

    prisma = sg.SceneGraphNode("prisma")
    prisma.transform = tr2.translate(0, 0, 10)
    prisma.childs += [prisma_rotated]
    return prisma

# Create image of ricardo
def createRicardo_1(filename):
    gpuAirport_texture = es.toGPUShape(bs.createTextureQuad(filename), GL_REPEAT, GL_LINEAR)
    ricardo_1_scaled = sg.SceneGraphNode("ricardo_scaled")
    ricardo_1_scaled.transform = tr2.scale(3, 3, 3)
    ricardo_1_scaled.childs += [gpuAirport_texture]

    ricardo_1_rotated = sg.SceneGraphNode("ricardo_rotated")
    ricardo_1_rotated.transform = np.matmul(tr2.rotationX(np.pi / 2), tr2.rotationZ(np.pi / 2))
    ricardo_1_rotated.childs += [ricardo_1_scaled]

    ricardo_1 = sg.SceneGraphNode("ricardo")
    ricardo_1.transform = tr2.translate(6, 0, 1)
    ricardo_1.childs += [ricardo_1_rotated]

    return ricardo_1

def trayectoria(Pi=np.array([[-5, -5, 5]]).T, Pf=np.array([[-5, -5, 5]]).T):
    T1 = np.array([[-50, 70, 7]]).T
    T2 = np.array([[70, 50, 7]]).T
    
    GMh = hermiteMatrix(Pi, Pf, T1, T2)
    # Number of samples to plot
    N = 100
    
    hermiteCurve = evalCurve(GMh, N)

    return hermiteCurve

def plane():
    c = [0.2, 0.5, 0.4]
    gpuSphere = es.toGPUShape(bs.createSphereTexture("image/towe_black.jpg",1), GL_REPEAT, GL_NEAREST)

    ricardo_1 = sg.SceneGraphNode("ricardo")
    ricardo_1.transform = tr2.scale(1, 4, .7)
    ricardo_1.childs += [gpuSphere]

    plane = sg.SceneGraphNode("plane")
    plane.transform = tr2.translate(10, 10, 10)
    plane.childs += [ricardo_1]

    return plane

def curvanodo():
    c = [80/255, 80/255, 90/255]
    vertices = [[1, 0], [0.9, 0.4], [0.5, 0.5], [0, 0.5]]
    curve = catrom.getSplineFixed(vertices, 10)
    obj_planeT = bs_ext.createPlaneFromCurveNode(curve, True, c[0], c[1], c[2])

    plane_t = sg.SceneGraphNode("tt_arbol")
    plane_t.transform = tr2.translate(.2,.2,0)
    plane_t.childs += [obj_planeT]

    plane = sg.SceneGraphNode("gg_arbol")
    dtheta = 2*np.pi/20
    for i in range(20):
        dt = i*dtheta
        nom = "move" + str(i)
        move = sg.SceneGraphNode(nom)
        move.transform = tr2.rotationZ(dt)
        move.childs += [plane_t]

        plane.childs += [move]

    ricardo_1 = sg.SceneGraphNode("ricardo")
    ricardo_1.transform = tr2.scale(10, 10, 1)
    ricardo_1.childs += [plane]

    tl = sg.SceneGraphNode("traslado")
    tl.transform = tr2.scale(.2,.2,0)
    tl.childs += [ricardo_1]
    
    return tl

if __name__ == "__main__":

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 600
    height = 600

    window = glfw.create_window(width, height, "Homework 2", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

    # tell GLFW to capture our mouse
    glfw.set_input_mode(window, glfw.CURSOR, glfw.CURSOR_DISABLED);

    # Connecting callback functions to handle mouse events:
    # - Cursor moving over the window
    # - Mouse buttons input
    # - Mouse scroll
    glfw.set_cursor_pos_callback(window, mouse_callback)
    glfw.set_scroll_callback(window, scroll_callback)

    # Assembling the shader program (pipeline) with shaders (simple, texture and lights)
    mvcPipeline = es.SimpleModelViewProjectionShaderProgram()
    textureShaderProgram = es.SimpleTextureModelViewProjectionShaderProgram()
    phongPipeline = es.SimplePhongShaderProgram()

    # Assembling the shader program
    # pipeline = es.SimpleModelViewProjectionShaderProgram()

    # Telling OpenGL to use our shader program
    glUseProgram(mvcPipeline.shaderProgram)

    # Setting up the clear screen color
    glClearColor(0.8, 0.8, 0.8, 1.0)

    # As we work in 3D, we need to check which part is in front,
    # and which one is at the back
    glEnable(GL_DEPTH_TEST)


    #poner archivos transparentes y 
    #glEnable(GL_BLEND)
    #glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    # Creating shapes on GPU memory
    '''
    gpuRedCube = es.toGPUShape(bs.createColorCube(1,0,0))
    gpuGreenCube = es.toGPUShape(bs.createColorCube(0,1,0))
    gpuBlueCube = es.toGPUShape(bs.createColorCube(0,0,1))
    gpuYellowCube = es.toGPUShape(bs.createColorCube(1,1,0))
    gpuCyanCube = es.toGPUShape(bs.createColorCube(0,1,1))
    gpuPurpleCube = es.toGPUShape(bs.createColorCube(1,0,1))
    gpuRainbowCube = es.toGPUShape(bs.createRainbowCube())
    '''
    gpuAxis = es.toGPUShape(bs.createAxis(7))
    redCarNode = createCar(252/255,246/255,246/255, 255/255, 153/255, 153/255, controller.lights)
    blueCarNode = createCar(252/255,246/255,246/255, 0, 76/255, 153/255, False)
    groundNode = createGround("image/suelo.png")
    ricardoNode = createRicardo_1("image/ricardo1.png")
    antena = antena()
    tower = createTower()
    cielo = createScene("image/city_2.jpg")
    mundo = createGroundSphere()
    esfera = createEsfera()
    cono = createCone()
    arbol = arbol()
    camino = createCamino("image/piso.jpg")
    cylinder = createCylinder()
    circle = createCircle()
    circle_t = createCircleTexture()
    plataforma = plataformas()
    plane = plane()
    #curvanodo = curvanodo()
    blueCarNode.transform = np.matmul(tr2.rotationZ(-np.pi/4), tr2.translate(3.0,0,0.5))

    #elise
    vertices = [[1, 0], [0.9, 0.4], [0.5, 0.5], [0, 0.5]]
    curve = catrom.getSplineFixed(vertices, 10)
    obj_planeT = bs_ext.createColorPlaneFromCurve(curve, True, 0.3, 0.5, .4)
    obj_planeT.setShader(mvcPipeline)
    obj_planeT.rotationX(np.pi/2)
    obj_planeT.rotationZ(np.pi/2)
    obj_planeT.rotationX(np.pi/7)
    obj_planeT.scale(sx=2.7,sy=2.5,sz=1)
    obj_planeT.translate(tx=10.,ty=9.5,tz=9.7)

    obj_planeC = bs_ext.createColorPlaneFromCurve(curve, False, 0.2, 0.5, 0.4, center=(0, 0))
    obj_planeC.setShader(mvcPipeline)
    obj_planeC.rotationZ(np.pi)
    obj_planeC.scale(sx=2.5,sy=2.5,sz=1)
    obj_planeC.translate(tx=10,ty=10.4,tz=10)

    obj_planeC_2 = bs_ext.createColorPlaneFromCurve(curve, False, 0.2, 0.5, 0.4, center=(0, 0))
    obj_planeC_2.setShader(mvcPipeline)
    obj_planeC_2.rotationX(np.pi)
    obj_planeC_2.scale(sx=2.5,sy=2.5,sz=1)
    obj_planeC_2.translate(tx=10,ty=10.4,tz=10)

    r = 10

    normal_view = tr2.lookAt(
            controller.cameraPos,
            controller.cameraPos + controller.cameraFront,
            controller.cameraUp
        )
    eye = np.array([5.0, 80.0,  6.0])
    at = np.array([0.0, -1.0, 0.0])
    up = np.array([0.0, 0.0,  1.0])

    normal_view = tr2.lookAt(eye,at,up)

    # lookAt of normal camera
    t0 = glfw.get_time()
    v = controller.firstMouse
    valor1 = 2*glfw.get_cursor_pos(window)
    while not glfw.window_should_close(window):
        glUseProgram(mvcPipeline.shaderProgram)
        valor = glfw.get_cursor_pos(window)
        #print("valor=" + str(valor))

        #curve = hermite_1_a()
        curve = trayectoria()
        paso = int(glfw.get_time()*20)%len(curve)

        pos = int(len(curve) * paso / 10)
        #print(paso)
        #mov circular auto 
        u_px = np.cos(glfw.get_time()/3)
        u_py = np.sin(glfw.get_time()/3)
        x = r * u_px
        y = r * u_py
        x_c = curve[paso][0]
        y_c = curve[paso][1]
        z_c = curve[paso][2]
        #print(str(x_c) + "," + str(y_c))

        # Getting the time difference from the previous iteration
        t1 = glfw.get_time()
        dt = t1 - t0
        t0 = t1
        if (glfw.get_key(window, glfw.KEY_W) == glfw.PRESS):
            controller.cameraPos += 30*dt * controller.cameraFront

        elif (glfw.get_key(window, glfw.KEY_S) == glfw.PRESS):
            controller.cameraPos -= 30*dt * controller.cameraFront
            #print(controller.cameraPos)


        elif (glfw.get_key(window, glfw.KEY_A) == glfw.PRESS):
            side = np.cross(controller.cameraFront, controller.cameraUp)
            controller.cameraPos -= side / np.linalg.norm(side) * 10 * dt

        elif (glfw.get_key(window, glfw.KEY_D) == glfw.PRESS):
            side = np.cross(controller.cameraFront, controller.cameraUp)
            controller.cameraPos += side / np.linalg.norm(side) * 10 * dt
        #lookAt(eye, at, up)
        view = tr2.lookAt(
            controller.cameraPos,
            controller.cameraPos + controller.cameraFront,
            controller.cameraUp
        )
        '''
        Si presionamos las teclas 1,2,3 o 4 podemos cambiar la vista de la camara a una estatica.
        PRECACION: EN LA VISTA ESTATICA, CUALQUIER MOVIMIENTO EN EL MOUSE O AL PRESIONAR LAS TECLAS A,W,S O D
        SE GENERA UN CAMBIO EN LA VISTA ORIGINAL (INICIAL).

        La camara helicoptero, se trabajó con angulos de euler(link), donde el movimiento de la camara se realiza
        con las teclas y el mouse en conjunto.

        '''
        if(controller.ChangeView):
            view = changeView(controller.POSCAM)
        '''
        print("eye = np.array(" + str(controller.cameraPos))
        print("at = np.array(" + str(controller.cameraFront))
        print("up = np.array(" + str(controller.cameraUp))
        print("controller.lastX = " + str(controller.lastX))
        print("controller.lastY = " + str(controller.lastY))
        print("controller.pitch = " + str(controller.pitch))
        print("controller.fov = " + str(controller.fov))
        print("controller.yaw = " + str(controller.yaw))
        '''
        if controller.show:
            obj_planeT.applyTemporalTransform(tr2.translate(x_c,y_c,z_c))
            obj_planeC_2.applyTemporalTransform(tr2.translate(x_c,y_c,z_c))
            obj_planeC.applyTemporalTransform(tr2.translate(x_c,y_c,z_c))
            plane.transform = np.matmul(tr2.translate(10, 10, 10),tr2.translate(x_c, y_c, z_c))
        # Moving the red car and rotating its wheels
        #redCarNode.transform = np.matmul(tr2.translate(0, 0, 0.5), tr2.translate(x_c, y_c, z_c))
        redCarNode.transform = np.matmul(tr2.translate(0, 0, 0.5), tr2.translate(x, y, 0))
        redCarNode.transform = np.matmul(redCarNode.transform, tr2.rotationZ(glfw.get_time()/3 + np.pi/2))
        #redWheelRotationNode = sg.findNode(redCarNode, "wheelRotation")
        #redWheelRotationNode.transform = tr2.rotationY(10 * glfw.get_time())

        projection = tr2.perspective(math.radians(controller.fov), float(width)/float(height), 1.0, 100)
        glUniformMatrix4fv(glGetUniformLocation(mvcPipeline.shaderProgram, "projection"), 1, GL_TRUE, projection)
        glUniformMatrix4fv(glGetUniformLocation(mvcPipeline.shaderProgram, "view"), 1, GL_TRUE, view)

        # Using GLFW to check for input events
        glfw.poll_events()

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # Filling or not the shapes depending on the controller state
        if (controller.fillPolygon):
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        else:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

        if controller.showAxis:
            glUniformMatrix4fv(glGetUniformLocation(mvcPipeline.shaderProgram, "model"), 1, GL_TRUE, tr2.identity())
            mvcPipeline.drawShape(gpuAxis, GL_LINES)

        if controller.ChangeView:
            changeView(controller.POSCAM)

        #dibujoavion
        if controller.show:
            obj_planeC.draw(view, projection)
            obj_planeC_2.draw(view, projection)
            obj_planeT.draw(view, projection)

        # Drawing shapes with different model transformations
        # glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "model"), 1, GL_TRUE, tr2.translate(5,0,0))
        # pipeline.drawShape(redCarNode)
        #sg.drawSceneGraphNode(cylinder, mvcPipeline)
        #sg.drawSceneGraphNode(cono, mvcPipeline)
        #sg.drawSceneGraphNode(esfera, mvcPipeline)
        #sg.drawSceneGraphNode(circle, mvcPipeline)
        #sg.drawSceneGraphNode(curvanodo, mvcPipeline)
        sg.drawSceneGraphNode(redCarNode, mvcPipeline)
        sg.drawSceneGraphNode(antena, mvcPipeline)
        sg.drawSceneGraphNode(arbol, mvcPipeline)
        sg.drawSceneGraphNode(blueCarNode, mvcPipeline)
        sg.drawSceneGraphNode(plataforma, mvcPipeline)
        
        
        # Drawing ground and ricardo using texture shader
        glUseProgram(textureShaderProgram.shaderProgram)
        glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "projection"), 1, GL_TRUE, projection)
        glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "view"), 1, GL_TRUE, view)
        # Drawing SceneGraphNodes
        if controller.show:
            sg.drawSceneGraphNode(esfera, textureShaderProgram)
            sg.drawSceneGraphNode(plane, textureShaderProgram)
        sg.drawSceneGraphNode(camino, textureShaderProgram)
        sg.drawSceneGraphNode(groundNode, textureShaderProgram)
        sg.drawSceneGraphNode(tower, textureShaderProgram)
        #sg.drawSceneGraphNode(circle_t,textureShaderProgram)
        if controller.changeGround:
            sg.drawSceneGraphNode(mundo, textureShaderProgram)
        else:
            sg.drawSceneGraphNode(cielo, textureShaderProgram)

        # Once the drawing is rendered, buffers are swap so an uncomplete drawing is never seen.
        glfw.swap_buffers(window)

    glfw.terminate()





'''#####################################################

    REF: https://learnopengl.com/Getting-started/Camera

    Cursores de movimiento con el teclado - Walk around
    
    Se cambia el valor de los vectores EYE, AT y UP iniciales
    cameraPos   = (0.0, 80.0,  0.0)
    cameraFront = (0.0, -1.0, 0.0)
    cameraUp    = (0.0, 0.0,  1.0)

    Se realiza modificando la distancia entre el cuerpo y la posicion del ojo 
    KEY_W: Realiza un acercamiento hacia la imagen
    KEY_S: Realiza un alejamiento de la imagen
    KEY_A: Mueve la imagen hacia la derecha
    KEY_D: Mueve la imagen hacia la izquierda


        elif (glfw.get_key(window, glfw.KEY_S) == glfw.PRESS):
    #if controller.cameraPos[2] >= 89.0:
        #controller.cameraPos[0] = 22.0
        #controller.cameraPos[1] = -0.15
        #controller.cameraPos[2] = 89.0
        #print(controller.cameraPos)
    controller.cameraPos -= 30*dt * controller.cameraFront
    #print(controller.cameraPos)

'''#####################################################


#True es transponer, antes de pasar de cpu agpu hay que transponer la imagen 
#glUniformMatrix4fv(glGetUniformLocation(mvcPipeline.shaderProgram, "view"), 1, GL_TRUE, view)

# Setting up the projection transform

#notar que 
#void glOrtho(GLdouble left,  GLdouble right,  GLdouble bottom,  GLdouble top,  GLdouble nearVal,  GLdouble farVal)
#if controller.projection == PROJECTION_ORTHOGRAPHIC:
#    projection = tr2.ortho(-8, 8, -8, 8, 0.1, 100)
#elif controller.projection == PROJECTION_FRUSTUM:
#    projection = tr2.frustum(-5, 5, -5, 5, 9, 100)
#elif controller.projection == PROJECTION_PERSPECTIVE:

'''
    print("controller.camPos = np.array(" + str(controller.cameraPos))
        print("controller.camFront = np.array(" + str(controller.cameraFront))
        print("controller.camUp = np.array(" + str(controller.cameraUp))
        print("controller.lastX = " + str(controller.lastX))
        print("controller.lastY = " + str(controller.lastY))
        print("controller.pitch = " + str(controller.pitch))
        print("controller.fov = " + str(controller.fov))
        print("controller.yaw = " + str(controller.yaw))

'''



