# coding=utf-8

import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import transformations as tr
import scene_graph_mod as sg
import sys
from sympy import sin, cos, acos
import random
import math


# We will use 32 bits data, so an integer has 4 bytes
# 1 byte = 8 bits
INT_BYTES    = 4

TR_SEN       = 0
TR_COS       = 1
TR_PARABOLA       = 2
TR_TRANSLATE = 3
TR_STANDARD  = 4
TR_UP        = 5
TR_VIBRATION = 6
TR_ACHICAR   = 7
TR_ZIGZAG = 8

# A class to store the application control
class Controller:
    def __init__(self):
        self.leftClickOn = False
        self.theta = 0.0
        self.mousePos = (0.0, 0.0)
    showTransform = TR_STANDARD
    fillPolygon = True
    useNight = False
    showFigure = False
    mouse_x=0.0
    mouse_y=0.0

# we will use the global controller as communication with the callback function
controller = Controller()

def cursor_pos_callback(window, x, y):
    global controller
    controller.mousePos = (x,y)


def mouse_button_callback(window, button, action, mods):

    global controller

    """
    glfw.MOUSE_BUTTON_1: left click
    glfw.MOUSE_BUTTON_2: right click
    glfw.MOUSE_BUTTON_3: scroll click
    """

    if (action == glfw.PRESS or action == glfw.REPEAT):
        if (button == glfw.MOUSE_BUTTON_1):
            controller.leftClickOn = True
            controller.mouse_x=glfw.get_cursor_pos(window)[0]/300
            controller.mouse_y=glfw.get_cursor_pos(window)[1]/300

        if (button == glfw.MOUSE_BUTTON_2):
            print("Mouse click - button 2:", glfw.get_cursor_pos(window)[0])

        if (button == glfw.MOUSE_BUTTON_3):
            print("Mouse click - button 3")

    elif (action ==glfw.RELEASE):
        if (button == glfw.MOUSE_BUTTON_1):
            controller.leftClickOn = False

def scroll_callback(window, x, y):

    print("Mouse scroll:", x, y)



def on_key(window, key, scancode, action, mods):
    global controller

    if action ==glfw.REPEAT or action == glfw.PRESS:
        if key == glfw.KEY_ENTER:
            print("Key_ENTER")
            controller.showFigure = not controller.showFigure

    if action != glfw.PRESS:
        return

    if key == glfw.KEY_SPACE:
        controller.fillPolygon = not controller.fillPolygon
        print("Toggle GL_FILL/GL_LINE")

    elif key == glfw.KEY_N:
        controller.useNight = not controller.useNight
        print("Key_ChangeWithDay")

    elif key == glfw.KEY_1:
        print('Translation')
        controller.showTransform = TR_TRANSLATE

    elif key == glfw.KEY_2:
        print('Translation')
        controller.showTransform = TR_SEN

    elif key == glfw.KEY_3:
        print('Translation')
        controller.showTransform = TR_COS

    elif key == glfw.KEY_4:
        print('Translation')
        controller.showTransform = TR_PARABOLA

    elif key == glfw.KEY_ESCAPE:
        sys.exit()


def getTransform(showTransform, theta, vInicial):

    if (showTransform == TR_STANDARD):
        return tr.identity()

    elif showTransform == TR_SEN:
        #ejex = 0.7 * np.sin(theta)
        sigma =theta*0.6
        vtheta = theta*3
        #print(0.8* np.sin(theta))
        #return tr.translate(0.8* np.sin(sigma), 0.2 * np.sin(vtheta), 0)

        if vInicial==1:
            return tr.translate(0.5 * np.sin(sigma), 0.2 * np.sin(vtheta), 0)
        elif vInicial==2:
            return tr.translate(-0.5 * np.sin(sigma), 0.2 * np.sin(vtheta), 0)
        elif vInicial==3:
            return tr.translate(-0.5 * np.sin(sigma), 0.2 * np.sin(vtheta), 0)
        elif vInicial==4:
            return tr.translate(0.5 * np.sin(sigma), 0.2 * np.sin(vtheta), 0)
        

    elif showTransform == TR_COS:
        sigma =theta*0.1
        #return tr.translate(0.5 * np.cos(theta), 0.7*(np.cos(sigma)), 0)

        if vInicial==1:
            return tr.translate(0.5 * np.cos(theta), 0.6 *(np.cos(sigma)), 0)
        elif vInicial==2:
            return tr.translate(-0.4 * np.cos(theta), 0.6 *(np.cos(sigma)), 0)
        elif vInicial==3:
            return tr.translate(-0.4 * np.cos(theta), 0.6 *(np.cos(sigma)), 0)
        elif vInicial==4:
            return tr.translate(0.5 * np.cos(theta), 0.6 *(np.cos(sigma)), 0)
            

    elif showTransform == TR_PARABOLA:
        vtheta = theta*1.2
        #return tr.translate(0.5 * np.cos(theta), -0.6 * abs(np.sin(theta)), 0)

        if vInicial==1:
            return tr.translate(0.37 * np.cos(theta), -0.5 * abs(np.sin(theta)), 0)
        elif vInicial==2:
            return tr.translate(0.37 * np.cos(theta), -0.5 * abs(np.sin(theta)), 0)
        elif vInicial==3:
            return tr.translate(0.37 * np.cos(theta), -0.5 * abs(np.sin(theta)), 0)
        elif vInicial==4:
            return tr.translate(0.37 * np.cos(theta), -0.5 * abs(np.sin(theta)), 0)
        

    elif showTransform == TR_ZIGZAG:
        vtheta = theta*0.7
        theta =theta*1.2
        #return tr.translate(0.8 * np.cos(vtheta), 0.3*abs(theta - math.floor(theta) - .5), 0)
        if vInicial==1:
            return tr.translate(0.4* np.cos(vtheta), 0.3*abs(theta - math.floor(theta) - .5), 0)
        elif vInicial==2:
            return tr.translate(-0.5* np.cos(vtheta), 0.3*abs(theta - math.floor(theta) - .5), 0)
        elif vInicial==3:
            return tr.translate(-0.5* np.cos(vtheta), 0.3*abs(theta - math.floor(theta) - .5), 0)
        elif vInicial==4:
            return tr.translate(0.4* np.cos(vtheta), 0.3*abs(theta - math.floor(theta) - .5), 0)

    elif showTransform == TR_TRANSLATE:
        #return tr.translate(0.5 * np.cos(theta), 0, 0)
        return tr.translate(0.5 * np.cos(theta), 0, 0)

    elif showTransform == TR_UP:
        #sigma es para que avance mas rapido
        sigma = (theta * 0.3)%3
        #theta es para ajustar la velocidad con que se encoje
        #theta = theta * 0.5
        #transform_neg = np.matmul(
        #    tr.uniformScale(0),
        #    tr.translate(0, sigma, 0)
        #    )
        #print(sigma)
        #print(np.cos(theta))
        transform_pos = np.matmul(
                tr.uniformScale(0.2),
                tr.translate(0, sigma, 0)
            )
        #if np.cos(theta)<0:
            #return transform_neg
        #else:
        return transform_pos 

    elif showTransform == TR_ACHICAR:
        transform = tr.uniformScale(0.6*abs(np.cos(theta)))
        return transform
    
    else:
        # This should NEVER happend
        raise Exception()


def createSky():
    # Here the new shape will be stored
    gpuShape = sg.GPUShape()

    # Defining locations and colors for each vertex of the shape
    vertexData = np.array([
        #   positions  colors
        -1, -1, 0, 0.7, 0.7, 0.8,
         1, -1, 0, 0.7, 0.7, 0.8,
         1,  1, 0, 0.1, 0.1, 0.7,
        -1,  1, 0, 0.1, 0.1, 0.7
        # It is important to use 32 bits data
    ], dtype=np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2,
         2, 3, 0], dtype=np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape


def createTriangle(v1, v2, color1,color2,color3):
    # Here the new shape will be stored
    gpuShape = sg.GPUShape() 

    # Defining the location and colors of each vertex  of the shape
    vertexData = np.array(
    #     positions       colors
        [ v1[0], v2[0], 0.0, color1[0], color1[1], color1[2],
          v1[1], v2[1], 0.0, color2[0], color2[1], color2[2],
          v1[2], v2[2], 0.0, color3[0], color3[1], color3[2]],
          dtype = np.float32) # It is important to use 32 bits data

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2], dtype= np.uint32)
        
    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape


def createQuad(v1,v2,color1,color2,color3,color4):

    # Here the new shape will be stored
    gpuShape = sg.GPUShape()

    # Defining locations and colors for each vertex of the shape
    
    vertexData = np.array([
    #   positions        colors
         v1[0],  v2[0], 0.0,  color1[0], color1[1], color1[2],
         v1[1],  v2[1], 0.0,  color2[0], color2[1], color2[2],
         v1[2],  v2[2], 0.0,  color3[0], color3[1], color3[2],
         v1[3],  v2[3], 0.0,  color4[0], color4[1], color4[2]
    # It is important to use 32 bits data
        ], dtype = np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2,
         2, 3, 0], dtype= np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def createCircle(R,n):

    gpuShape=sg.GPUShape()

    vertexData=np.array(
        [0.0, 0.0, 0.0, 0.5, 0.5, 0.5],
        dtype=np.float32)

    dtheta = 2*np.pi/n
    for i in range(n):
        theta = i * dtheta
        inter = (2*np.pi)/6
        inverInter = inter**(-1)
        if 0 <= theta <= inter:
            vertexData = np.append(vertexData, np.array([R*cos(theta),R*sin(theta),0.0, 1.0, abs(sin((np.pi/2)*theta*inverInter)), 0.0],dtype=np.float32))

        elif inter < theta <= 2*inter:
            vertexData = np.append(vertexData, np.array([R*cos(theta),R*sin(theta),0.0, abs(sin((np.pi/2)*theta*inverInter)), 1.0, 0],dtype=np.float32))

        elif 2*inter < theta <= 3*inter:
            vertexData = np.append(vertexData, np.array([R*cos(theta),R*sin(theta),0.0, 0.0, 1.0, abs(sin((np.pi/2)*theta*inverInter))], dtype=np.float32))

        elif 3*inter <= theta <= 4*inter:
            vertexData = np.append(vertexData, np.array([R*cos(theta),R*sin(theta),0.0, 0.0, abs(sin((np.pi/2)*theta*inverInter)), 1.0], dtype=np.float32))

        elif 4*inter < theta <= 5*inter:
            vertexData = np.append(vertexData, np.array([R*cos(theta),R*sin(theta),0.0, abs(sin((np.pi/2)*theta*inverInter)), 0.0, 1.0], dtype=np.float32))

        elif 5*inter < theta < 6*inter:
            vertexData = np.append(vertexData, np.array([R*cos(theta),R*sin(theta),0.0, 1.0, 0.0, abs(sin((np.pi/2)*theta*inverInter))], dtype=np.float32))

    indices = np.array([],dtype=np.uint32)
    
    for i in range(1,n):
        indices = np.append(indices,np.array([0,i,i+1],dtype=np.uint32))
    indices = np.append(indices,np.array([0,n,1],dtype=np.uint32))

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def createCircleColor(factorPi,colorCentral,R,n,color):

    gpuShape=sg.GPUShape()

    vertexData=np.array(
        [0.0, 0.0, 0.0, colorCentral[0], colorCentral[1], colorCentral[2]],
        dtype=np.float32)

    dtheta = factorPi*np.pi/n
    for i in range(n):
        theta = i * dtheta
        vertexData = np.append(vertexData, np.array([R*cos(theta),R*sin(theta),0.0, color[0], color[1], color[2]],dtype=np.float32))

    indices = np.array([],dtype=np.uint32)
    
    for i in range(1,n):
        indices = np.append(indices,np.array([0,i,i+1],dtype=np.uint32))
    indices = np.append(indices,np.array([0,n,1],dtype=np.uint32))

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape


def createDunes(color1,color2,color3):
    # Here the new shape will be stored
    gpuShape = sg.GPUShape()
    n = random.random()
    if n<0.2:
    # Defining locations and colors for each vertex of the shape
        vertexData = np.array([
            #   positions    colors
            -n-1, -1, 0, color1[0], color1[1], color1[2],
            n+1, -1, 0, color2[0], color2[1], color2[2],
            -n-0.8, -.5, 0, color3[0], color3[1], color3[2],
            # It is important to use 32 bits data
        ], dtype=np.float32)
    elif 0.2<n<0.4:
        vertexData = np.array([
            #   positions    colors
            -n-1, -1, 0, color1[0], color1[1], color1[2],
            n+1, -1, 0, color2[0], color2[1], color2[2],
            -n-0.3, -.5, 0, color3[0], color3[1], color3[2],
            # It is important to use 32 bits data
        ], dtype=np.float32)

    elif 0.4<n<0.6:
        vertexData = np.array([
            #   positions    colors
            -n-1, -1, 0, color1[0], color1[1], color1[2],
            n+1, -1, 0, color2[0], color2[1], color2[2],
            n-0.3, -.5, 0, color3[0], color3[1], color3[2],
            # It is important to use 32 bits data
        ], dtype=np.float32)

    elif 0.6<n<0.8:
        vertexData = np.array([
            #   positions    colors
            -n-1, -1, 0, color1[0], color1[1], color1[2],
            n+1, -1, 0, color2[0], color2[1], color2[2],
            n-0.3, -.5, 0, color3[0], color3[1], color3[2],
            # It is important to use 32 bits data
        ], dtype=np.float32)

    else:
        vertexData = np.array([
            #   positions    colors
            -n-1, -1, 0, color1[0], color1[1], color1[2],
            n+1, -1, 0, color2[0], color2[1], color2[2],
            n-0.2, -.5, 0, color3[0], color3[1], color3[2],
            # It is important to use 32 bits data
        ], dtype=np.float32)


    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2], dtype=np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def createCircleWithTriang(lx,ly,color1,color2,color3,escala):
    
    tres = []
    v1 =[0.3, 0, 0.3]
    v2 =[-0.01, 0, 0.01]
    for i in range(100):
        angle = i * 2 * np.pi/100
        nom = "triangle_" + str(i)
        tria = sg.SceneGraphNode(nom)
        tria.transform = tr.rotationZ(angle)
        tria.childs += [createTriangle(v1,v2,color1,color2,color3)]
        tres.append(tria)

    circleTree = sg.SceneGraphNode("circleTree")
    for i in range(len(tres)):
        circleTree.childs += [tres[i]]

    circleTreeScale = sg.SceneGraphNode("circleTreeScale")
    circleTreeScale.transform = tr.scale(escala[0],escala[1],1)
    circleTreeScale.childs += [circleTree]

    circleTranslate = sg.SceneGraphNode("circleTranslate")
    circleTranslate.transform = tr.translate(lx,ly,1)
    circleTranslate.childs += [circleTreeScale]

    return circleTranslate


def createRock(lx,ly,escala):
    color1 = [0.1,.2,0.2]
    color2 = [0.1,.2,0.2]
    color3 = [0.1,.2,0.2]
    roca = createCircleWithTriang(lx,ly,color1,color2,color3,escala)
    rock = sg.SceneGraphNode("rock")
    rock.childs += [roca]

    return rock

def createBurbble(lx,ly,num):
    color1 = [0.4,.3,1]
    color2 = [0.5,.5,0.8]
    color3 = [0.4,.3,1]
    blue = [.3, .5 , .9]
    #burbuja = createCircleWithTriang(lx,ly,color1,color2,color3,escala)
    burbuja = createCircleColor(2,[0.5,0.5,0.5],0.2,100,blue)

    bubble = sg.SceneGraphNode("burbble_" +str(num))
    bubble.childs += [burbuja]

    bubbleTranslate = sg.SceneGraphNode("burbble_t")
    bubbleTranslate.transform = tr.translate(lx,ly,0)
    bubbleTranslate.childs += [bubble]

    return bubbleTranslate


def createAlga(lx,ly,scale):
    color1 = [0,0.75,0]
    color2 = [0.3,0.9,0.3]#y=0.9
    color3 = [0.2,0.6,0.2] 
    color4 = [0,0.75,0] 
    v1 = [-0.1,0.1,0.1,-0.1]
    v2 = [-0.1,-0.1,0.1,0.1]
    quad = createQuad(v1,v2,color1,color2,color3,color4)

    quadObjet = sg.SceneGraphNode("quadGreen")
    quadObjet.transform = tr.shearing(0.3, 0, 0, 0, 0, 0)
    quadObjet.childs  += [quad]

    quadTranslate_2 = sg.SceneGraphNode("quadTranslate_2")
    quadTranslate_2.transform = tr.translate(0.06,0.2,0)
    quadTranslate_2.childs  += [quad]

    quadObjet_2 = sg.SceneGraphNode("quadGreen_2")
    quadObjet_2.transform = tr.shearing(-0.3, 0, 0, 0, 0, 0)
    quadObjet_2.childs  += [quadTranslate_2]

    alga = sg.SceneGraphNode("alga")
    #alga.transform = tr.translate(0.3, -0.6, 0)
    alga.childs  += [quadObjet]
    alga.childs  += [quadObjet_2]

    arrayAlgas =[]
    cant=8
    #range, numero de hojas de la alga
    for i in range(1,cant):
        h = i * 0.4
        nom = "translateAlga_" + str(i)
        translateAlga = sg.SceneGraphNode(nom)
        translateAlga.transform = tr.translate(0,h,0)
        translateAlga.childs += [alga]
        if i>=5:
            nom_scale = "scaleAlga_" + str(i)
            scaleAlga = sg.SceneGraphNode(nom_scale)
            scaleAlga.transform = tr.scale(0.4*abs(i-cant+1),1,1)
            scaleAlga.childs += [translateAlga]
            arrayAlgas.append(scaleAlga)
        else:
            arrayAlgas.append(translateAlga)

    algaCompleta = sg.SceneGraphNode("algaCompleta")
    #algaCompleta.transform = tr.translate(-0.6, -0.7, 0)
    algaCompleta.childs  += [quadObjet]
    algaCompleta.childs  += [quadObjet_2]
    for i in range(len(arrayAlgas)):
        algaCompleta.childs += [arrayAlgas[i]]

    algaScale = sg.SceneGraphNode("algaScale")
    algaScale.transform = tr.uniformScale(scale)
    algaScale.childs  += [algaCompleta]

    algaCompletaTranslate = sg.SceneGraphNode("algaCompletaTranslate")
    algaCompletaTranslate.transform = tr.translate(lx,ly,0)
    algaCompletaTranslate.childs  += [algaScale]

    return algaCompletaTranslate

def createAlgab():
    algaRot = sg.SceneGraphNode("algaRot")
    algaRot.transform = tr.rotationZ(np.pi/4)
    algaRot.childs  += [algaCompleta]

    quadScale_2 = sg.SceneGraphNode("quadScale_2")
    quadScale_2.transform = tr.uniformScale(0.6)
    quadScale_2.childs  += [algaRot]

    algaScale = sg.SceneGraphNode("algaScale")
    algaScale.transform = tr.uniformScale(0.2)
    algaScale.childs  += [algaCompleta]
    algaScale.childs  += [quadScale_2]

    return algaScale


"""
COLORES 


    BODY FISH
    color1 = [1,.7,0]
    color2 = [.8,.5,0]
    color3 = [1,.7,0]

    EYES FISH


    TAIL FISH
    color1 = [1,.8,0]
    color2 = [.8,1,0]
    color3 = [1,.8,0]

    -0.5, -0.5, 0.0,  color1[0], color1[1], color1[2],
         0.5, -0.5, 0.0,  color2[0], color2[1], color2[2],
         0.5,  0.5, 0.0,  color3[0], color3[1], color3[2],
        -0.5,  0.5, 0.0, 1, 1, 1


    ROCA
    color1 = [0.1,.2,0.2]
    color2 = [0.1,.2,0.2]
    color3 = [0.1,.2,0.2]

"""


def bodyFish():
    tres=[]
    v1 =[0.23, 0, 0.23]
    v2 =[-0.1, 0, 0.1]
    color1 = [1,.7,0]
    color2 = [.8,.5,0]
    color3 = [1,.7,0]
    for i in range(8):
        angle = i * np.pi/4
        nom = "triangle_" + str(i)
        tria = sg.SceneGraphNode(nom)
        tria.transform = tr.rotationZ(angle)
        tria.childs += [createTriangle(v1,v2,color1,color2,color3)]
        tres.append(tria)

    triang = sg.SceneGraphNode("Triangulo")
    for i in range(len(tres)):
        triang.childs += [tres[i]]

    return triang

def bodyFishGral(color):
    n = 30
    R=0.5
    circulo = createCircleColor(2,[0.5,0.5,0.5],R,n,color)

    circle = sg.SceneGraphNode("circle")
    circle.transform = tr.uniformScale(0.5)
    circle.childs += [circulo]

    circleScale = sg.SceneGraphNode("circleScale")
    circleScale.transform = tr.scale(1,0.6,1)
    circleScale.childs += [circle]

    return circleScale


def tailFish(color1,color2,color3):
    v1 =[0.23, 0, 0.23]
    v2 =[-0.1, 0, 0.1]
    colaList =[]
    colaNom =[]
    for i in range(4):
        cola = createTriangle(v1,v2,color1,color2,color3)
        colaList.append(cola) 
        nom = "aleta_"+str(i)
        aleta =sg.SceneGraphNode(nom)
        colaNom.append(aleta)

    #for i in range(4):
        #colaNom[i].transform = tr.rotationZ()

    colaNom[0].transform = tr.rotationZ(0.6)
    colaNom[1].transform = tr.rotationZ(0.2)
    colaNom[2].transform = tr.rotationZ(-0.2)
    colaNom[3].transform = tr.rotationZ(-0.6)

    for i in range(4):
        colaNom[i].childs = [colaList[i]]

    colaFish = sg.SceneGraphNode("cola")
    colaFish.transform = tr.translate(0.17,0,0)
    for i in range(4):
        colaFish.childs += [colaNom[i]]

    return colaFish

#aletas
def finsFish():
    v1=[-.4, .4, .4, -.4]
    v2=[-.4, -.6, .6, .4]
    color1 = [0.1, .2, 0]
    color2 = [0.2, .2, 0.5]
    color3 = [0.2, .2, 0.5]
    color4 = [0.1, .2, 0]
    quad = createQuad(v1,v2,color1,color2,color3,color4)

    finsScaleY = sg.SceneGraphNode("finsScaleY")
    finsScaleY.transform = tr.scale(0.2,0.6,1)
    finsScaleY.childs += [quad]

    fins = sg.SceneGraphNode("fins")
    fins.transform = tr.uniformScale(0.2)
    fins.childs += [finsScaleY]

    return fins

def eyesFish():
    tres=[]
    v1 =[0.23, 0, 0.23]
    v2 =[-0.1, 0, 0.1]
    color1 = [1,1,1]
    color2 = [0.2,0.2,0.2]
    color3 = [1,1,1]
    for i in range(8):
        angle = i * np.pi/4
        nom = "triangleEyes_" + str(i)
        tria = sg.SceneGraphNode(nom)
        tria.transform = tr.rotationZ(angle)
        tria.childs = [createTriangle(v1,v2,color1,color2,color3)]
        tres.append(tria)

    triang = sg.SceneGraphNode("triangleEyes")
    for i in range(len(tres)):
        triang.childs += [tres[i]]

    eyesScale = sg.SceneGraphNode("eyesScale")
    eyesScale.transform = tr.uniformScale(0.15)
    eyesScale.childs += [triang]

    eyesScaleY = sg.SceneGraphNode("eyesScaleY")
    eyesScaleY.transform = tr.scale(0.7,1,1)
    eyesScaleY.childs += [eyesScale]

    eyes = sg.SceneGraphNode("eyes")
    eyes.transform = tr.translate(-0.11,0.05,0)
    eyes.childs += [eyesScaleY]

    return eyes

def createFishGral(lx,ly,escala,colorBody,nom):  
    body = bodyFishGral(colorBody)
    bodyPez = sg.SceneGraphNode("bodyPez")
    bodyPez.childs += [body]

    v1=[-.4, .4, .4, -.4]
    v2=[-.4, -.5, .6, .4]
    color1 = [0.1, .2, 0]
    color2 = [0.2, .2, 0.5]
    color3 = [0.2, .2, 0.5]
    color4 = [0.1, .2, 0]
    quadFins = createQuad(v1,v2,color1,color2,color3,color4)

    quadFinsScale=sg.SceneGraphNode("quadFinsScale")
    quadFinsScale.transform = tr.scale(0.4, 0.8, 1)
    quadFinsScale.childs += [quadFins]

    quadFinsUniformScale=sg.SceneGraphNode("quadFinsUniformScale")
    quadFinsUniformScale.transform = tr.uniformScale(0.53)
    quadFinsUniformScale.childs += [quadFinsScale]
    
    colorCola = [[.7,.2,0],
    [.8,1,0],
    [.7,.2,0]]

    cola = sg.SceneGraphNode("cola")
    cola.transform = tr.scale(1,0.6,1)
    cola.childs += [tailFish(colorCola[0],colorCola[1],colorCola[2])]


    fins = finsFish()
    eyes = eyesFish()
    fins2= finsFish()
    fins3= finsFish()

    finsTrans=sg.SceneGraphNode("finsTrans")
    finsTrans.transform = tr.translate(0.12,0,0)
    finsTrans.childs += [fins2]

    finsTrans2=sg.SceneGraphNode("finsTrans2")
    finsTrans2.transform = tr.translate(0.06,0,0)
    finsTrans2.childs += [fins3]

    fish = sg.SceneGraphNode("fisher")
    fish.childs += [quadFinsUniformScale]
    fish.childs += [cola]
    fish.childs += [bodyPez]
    fish.childs += [fins]
    fish.childs += [eyes]
    fish.childs += [finsTrans]
    fish.childs += [finsTrans2]

    baseNom = nom
    pez = sg.SceneGraphNode("s_" +baseNom)
    pez.transform = tr.uniformScale(escala)
    pez.childs += [fish] 

    pezTranslate = sg.SceneGraphNode("t_" +baseNom)
    pezTranslate.transform = tr.translate(lx,ly,0)
    pezTranslate.childs += [pez]

    return pezTranslate


def createFish(lx,ly,escala,nom):

    body = bodyFish()
    bodyPez = sg.SceneGraphNode("bodyPez")
    bodyPez.transform = tr.scale(1,0.7,1)
    bodyPez.childs += [body]

    v1=[-.4, .4, .4, -.4]
    v2=[-.4, -.5, .6, .4]
    color1 = [0.1, .2, 0]
    color2 = [0.2, .2, 0.5]
    color3 = [0.2, .2, 0.5]
    color4 = [0.1, .2, 0]
    quadFins = createQuad(v1,v2,color1,color2,color3,color4)

    quadFinsScale=sg.SceneGraphNode("quadFinsScale")
    quadFinsScale.transform = tr.scale(0.4, 0.8, 1)
    quadFinsScale.childs += [quadFins]

    quadFinsUniformScale=sg.SceneGraphNode("quadFinsUniformScale")
    quadFinsUniformScale.transform = tr.uniformScale(0.6)
    quadFinsUniformScale.childs += [quadFinsScale]
    
    colorCola = [[1,.8,0],
    [.8,1,0],
    [1,.8,0]]

    cola = tailFish(colorCola[0],colorCola[1],colorCola[2])
    fins = finsFish()
    eyes = eyesFish()
    fins2= finsFish()
    fins3= finsFish()

    finsTrans=sg.SceneGraphNode("finsTrans")
    finsTrans.transform = tr.translate(0.12,0,0)
    finsTrans.childs += [fins2]

    finsTrans2=sg.SceneGraphNode("finsTrans2")
    finsTrans2.transform = tr.translate(0.06,0,0)
    finsTrans2.childs += [fins3]

    fish = sg.SceneGraphNode("fisher")
    fish.childs += [quadFinsUniformScale]
    fish.childs += [cola]
    fish.childs += [bodyPez]
    fish.childs += [fins]
    fish.childs += [eyes]
    fish.childs += [finsTrans]
    fish.childs += [finsTrans2]

    baseNom = nom
    pez = sg.SceneGraphNode("s_" +baseNom)
    pez.transform = tr.uniformScale(escala)
    pez.childs += [fish] 

    pezTranslate = sg.SceneGraphNode("t_" + baseNom)
    pezTranslate.transform = tr.translate(lx,ly,0)
    pezTranslate.childs += [pez]

    return pezTranslate

def createGround():

    # Here the new shape will be stored
    gpuShape = sg.GPUShape()

    # Defining locations and colors for each vertex of the shape
    vertexData = np.array([
    #   positions    colors
        -1,   -1, 0, 160/255.0, 134/255.0, 73/255.0,
         1,   -1, 0, 168/255.0, 121/255.0, 11/255.0,
         1, -0.52, 0, 244/255.0, 223/255.0, 66/255.0,
        -1, -0.52, 0,         1, 207/255.0, 96/255.0
    # It is important to use 32 bits data
        ], dtype = np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2,
         2, 3, 0], dtype= np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape


def createArena():
    contador=30
    DunesList = []
    color1=[160 / 255.0, 134 / 255.0, 73 / 255.0]
    color2=[168 / 255.0, 121 / 255.0, 11 / 255.0]
    color3=[244 / 255.0, 223 / 255.0, 66 / 255.0]
    while(contador>0):
        DunesList.append(createDunes(color1,color2,color3))
        contador-=1

    arena = sg.SceneGraphNode("arena")
    arenaList = []
    for i in range(len(DunesList)):
        nom = "dune" + str(i)
        dune = sg.SceneGraphNode(nom)
        dune.childs += [DunesList[i]]
        arenaList.append(dune)

    for i in arenaList:
        arena.childs += [i]

    return arena

def createTypeAlga(numero,rx,ry,scalex,scaley):
    alga = createAlga(0.0,0.0,0.3)
    algaPpal = sg.SceneGraphNode("algaPpal")
    algaPpal.transform = tr.translate(0.3,-0.7,0)
    algaPpal.childs += [alga]
    if numero ==1:
        dt = 0
        dc = 0
        lal = []
        algaTotal = sg.SceneGraphNode("algaTotal")
        algaTotal.childs += [alga]
        for i in range(4):
            hijoAlga_scale = sg.SceneGraphNode("hijoAlga_scale_" + str(i))
            hijoAlga_scale.transform = tr.scale(1,0.7-dc,1)
            hijoAlga_scale.childs += [alga]

            hijoAlga = sg.SceneGraphNode("hijoAlga_" + str(i))
            hijoAlga.transform = tr.translate(-0.05-dt,0,0)
            hijoAlga.childs += [hijoAlga_scale]
            lal.append(hijoAlga)
            dt+=0.06
            dc+=0.1
        dt=0
        dc=0
        for i in range(4):
            hijoAlga_scale = sg.SceneGraphNode("hijoAlga_scale_" + str(i+5))
            hijoAlga_scale.transform = tr.scale(1,0.7-dc,1)
            hijoAlga_scale.childs += [alga]
            hijoAlga = sg.SceneGraphNode("hijoAlga_" + str(i+5))
            hijoAlga.transform = tr.translate(0.05+dt,0,0)
            hijoAlga.childs += [hijoAlga_scale]
            lal.append(hijoAlga)
            dt+=0.06
            dc+=0.1

        for i in range(8):
            algaTotal.childs += [lal[i]]

        megaA = sg.SceneGraphNode("megaA_scale")
        megaA.transform = tr.scale(scalex,scaley,1)
        megaA.childs += [algaTotal]

        algaTotalTras = sg.SceneGraphNode("algaTotalTras")
        algaTotalTras.transform = tr.translate(rx,ry,0)
        algaTotalTras.childs += [megaA]

        return algaTotalTras
    
    elif numero ==2:
        dt = 0
        dc = 0
        lal = []
        algaTotal = sg.SceneGraphNode("algaTotal")
        algaTotal.childs += [alga]
        for i in range(4):
            hijoAlga_scale = sg.SceneGraphNode("hijoAlga_scale_" + str(i))
            hijoAlga_scale.transform = tr.scale(1,0.7-dc,1)
            hijoAlga_scale.childs += [alga]

            hijoAlga = sg.SceneGraphNode("hijoAlga_" + str(i))
            hijoAlga.transform = tr.rotationZ(np.pi/20+dt)
            hijoAlga.childs += [hijoAlga_scale]
            lal.append(hijoAlga)
            dt+=np.pi/20
            dc+=0.1
        dt=0
        dc=0
        for i in range(4):
            hijoAlga_scale = sg.SceneGraphNode("hijoAlga_scale_" + str(i+5))
            hijoAlga_scale.transform = tr.scale(1,0.7-dc,1)
            hijoAlga_scale.childs += [alga]

            hijoAlga = sg.SceneGraphNode("hijoAlga_" + str(i+5))
            hijoAlga.transform = tr.rotationZ(-np.pi/20-dt)
            hijoAlga.childs += [hijoAlga_scale]
            lal.append(hijoAlga)
            dt+=np.pi/20
            dc+=0.1

        for i in range(8):
            algaTotal.childs += [lal[i]]

        megaA = sg.SceneGraphNode("megaA_scale")
        megaA.transform = tr.scale(scalex,scaley,1)
        megaA.childs += [algaTotal]

        algaTotalTras = sg.SceneGraphNode("algaTotalTras")
        algaTotalTras.transform = tr.translate(rx,ry,0)
        algaTotalTras.childs += [megaA]

        return algaTotalTras

    else:
        return createAlga(-0.6,-0.6,0.1)


#cursor es bool
def cardumen(cantidad):
    t1 = TR_SEN
    t2 = TR_COS
    t3 = TR_UP
    cardumen = sg.SceneGraphNode("cardumen")
    cardumenList = []
    for i in range(cantidad):
        rx=random.uniform(-.6,.6)
        ry=random.uniform(-.6,.6)
        lugar = [rx,ry]
        #print(lugar)
        scale = random.uniform(0.2,.4)
        nom = "fish_numero_" + str(i)
        #a cierto tipo de pez lo translado a un lado y a otro tipo lo translado al otro lado 
        if -0.1<ry<0.4:
            fish = sg.SceneGraphNode(nom)
            fish.childs += [createFish(lugar[0],lugar[1],scale,nom)]

        else:
            red = [.9, .1, 0]
            blue = [0, .1 , .8]
            orange = [.9, .5, 0]
            yellow = [.9, .7, 0]
            green = [.3, 1.0, 0]
            black = [0,0,0]
            color = random.choice([red,blue,orange,yellow,green,black])
            fish = sg.SceneGraphNode(nom)
            fish.childs += [createFishGral(lugar[0],lugar[1],scale,color,nom)]

        cardumenList.append(fish)

    for i in range(len(cardumenList)):
        cardumen.childs +=[cardumenList[i]]

    return cardumen

def createBarco():
    v1=[-0.15,0.15, 0.15, -0.15]
    v2=[-0.8,-0.8, -0.1, 0.1]
    color=[0.4,0.17,0.04,0.04]

    quad = sg.SceneGraphNode("quad_barco")
    quad.transform = tr.translate(0.15,0.06,0)
    quad.childs += [createQuad(v1,v2,color,color,color,color)]

    v3=[-0.2,0.1, 0.1, -0.2]
    v4=[-0.15,-0.15, 0.1, 0.1]

    rect = sg.SceneGraphNode("rect_barco")
    #rect.transform = tr.translate(0.15,0.06,0)
    rect.childs += [createQuad(v3,v4,color,color,color,color)]

    palo = sg.SceneGraphNode("palo_barco")
    palo.transform = tr.scale(2.2,0.2,1)
    palo.childs += [createQuad(v3,v4,color,color,color,color)]

    palo_chico = sg.SceneGraphNode("palo_chico_barco")
    palo_chico.transform = tr.scale(0.4 ,0.1,1)
    palo_chico.childs += [createQuad(v3,v4,color,color,color,color)]

    palo_chicot = sg.SceneGraphNode("palo_chicot_barco")
    palo_chicot.transform = tr.translate(0,0.285,0)
    palo_chicot.childs += [palo_chico]

    asta = sg.SceneGraphNode("asta")
    dt = 0
    for i in range(3):
        nom = "paloCruzado_barco_" + str(i) 
        paloC = sg.SceneGraphNode(nom)
        paloC.transform = tr.scale(0.13,1.0+dt*1.5,1)
        paloC.childs += [createQuad(v3,v4,color,color,color,color)]

        paloCscale = sg.SceneGraphNode("paloCruzadoScale_barco_" + str(i))
        paloCscale.transform = tr.translate(-0.3+dt,0.03,0)
        paloCscale.childs += [paloC]
        asta.childs += [paloCscale]
        dt+=0.1
    asta.childs +=[palo]

    quad_rotate = sg.SceneGraphNode("quad_rotate_barco")
    quad_rotate.childs += [quad]

    color=[0.4,0.17,0.04]
    circle = sg.SceneGraphNode("circle_barco")
    circle.transform = tr.rotationZ(-np.pi/2)
    #circle.transform = tr.translate(-0.3,-0.3,0)
    circle.childs += [createCircleColor(1.025,[0.5,0.3,0.1],0.3,40,color)]

    barco = sg.SceneGraphNode("barco")
    barco.transform = tr.rotationZ(-np.pi/4)
    barco.childs += [palo_chicot]
    #barco.childs += [rect]
    barco.childs += [asta]
    barco.childs += [quad]
    barco.childs += [circle]

    ruina = sg.SceneGraphNode("ruina")
    ruina.transform = tr.translate(-0.4,-0.4,0)
    #ruina.childs += [rect]
    ruina.childs += [barco]

    return ruina


def createWorld(numeroFish):

    sky = sg.SceneGraphNode("sky")
    sky.childs += [createSky()]

    ground = sg.SceneGraphNode("ground")
    ground.childs += [createArena()]

    poblacion = cardumen(numeroFish)

    burbujas = sg.SceneGraphNode("burbujas")
    for i in range(10):
        random.uniform
        rx=random.uniform(-1.0,1.0)
        ry=random.uniform(-2,-1.2)
        name = sg.SceneGraphNode("b_name_" + str(i))
        name.childs += [createBurbble(rx,ry, i)] 
        burbujas.childs += [name]
    barco = createBarco()

    megaA1 = createTypeAlga(1,0.3,-0.8,0.4,0.25)
    megaA2 = createTypeAlga(2,-0.3,-0.8,0.4,0.25)
    megaA3 = createTypeAlga(2,-0.9,-0.9,0.4,0.25)
    #alga = sg.SceneGraphNode("alga_scale")
    #alga.transform = tr.scale()
    #alga.childs += [createAlga(-0.8,-0.8,0.1)]

    world = sg.SceneGraphNode("world")
    world.childs += [sky]
    world.childs += [barco]
    world.childs += [burbujas]
    world.childs += [ground]
    world.childs += [megaA3]
    world.childs += [createAlga(-0.6,-0.6,0.1)]
    world.childs += [createAlga(0.6,-0.6,0.15)]
    world.childs += [createAlga(0.1,-0.8,0.1)]
    world.childs += [createAlga(-0.8,-0.8,0.15)]
    escala = [0.25, 0.15]
    escala1 = [0.20, 0.10]
    world.childs += [createRock(0.4,-0.6, escala)]
    world.childs += [createRock(-0.4,-0.6, escala1)]
    world.childs += [poblacion]

    return world


if __name__ == "__main__":

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 600
    height = 600

    window = glfw.create_window(width, height, "Mundo Maritimo", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

    # Connecting callback functions to handle mouse events:
    # - Cursor moving over the window
    # - Mouse buttons input
    # - Mouse scroll
    glfw.set_cursor_pos_callback(window, cursor_pos_callback)
    glfw.set_mouse_button_callback(window, mouse_button_callback)
    glfw.set_scroll_callback(window, scroll_callback)

    boole= True
    # Assembling the shader program (pipeline) with both shaders
    shaderProgram = sg.basicShaderProgram(not boole)
    
    # Telling OpenGL to use our shader program
    glUseProgram(shaderProgram)

    # Setting up the clear screen color
    glClearColor(0.15, 0.15, 0.15, 1.0)

    # Creating shapes on GPU memory
    numeroFish = 6
    world = createWorld(numeroFish)


    # Our shapes here are always fully painted
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

    tt1 = TR_SEN
    tt2 = TR_COS
    tt3 = TR_PARABOLA
    tt4 = TR_ZIGZAG

    lista =[]
    x=0

    while not glfw.window_should_close(window):
        # Using GLFW to check for input events
        glfw.poll_events()
        if (controller.fillPolygon):
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        else:
            glPolygonMode(GL_FRONT_AND_BACK, GL_POINT)

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT)

        theta = glfw.get_time()

        nodeWorld = sg.findNode(world, "cardumen")
        if controller.showFigure:
            controller.showFigure = not controller.showFigure
            rx=random.uniform(-.6,.6)
            ry=random.uniform(-.6,.6)
            red = [.9, .1, 0]
            blue = [0, .1 , .8]
            orange = [.9, .5, 0]
            yellow = [.9, .7, 0]
            green = [.3, 1.0, 0]
            black = [0,0,0]
            color = random.choice([red,blue,orange,yellow,green,black])
            #el numero del pescado no hay que cambiarlo porque como son 4 pescado, el ultimo tiene numero 3, luego el nuevo va a tener el numero de los pescados que queriamos agregar
            nom = "fish_numero_" + str(numeroFish)
            #print(nom)
            nuevoFish = sg.SceneGraphNode(nom)
            scale = random.uniform(0.2,.4)
            nuevoFish.childs += [createFishGral(rx, ry, scale,color,nom)]

            nodeWorld.childs += [nuevoFish]
            numeroFish+=1

        for i in range(numeroFish):
            ver = 0
            nom = "fish_numero_" + str(i)
            fish =sg.findNode(world, nom)
            trans = sg.findTransform(fish,"t_" + nom,tr.identity())
            if x<numeroFish:
                vInicialFish = [trans[3][0],trans[3][1]]
                lista.append(vInicialFish)
            valor = lista[i]
            if valor[0]>0 and valor[1]>0:
                ver=1
            elif valor[0]<0 and valor[1]>0:
                ver=2
            elif valor[0]<0 and valor[1]<0:
                ver=3
            elif valor[0]>0 and valor[1]<0:
                ver=4

            if i%4==0:
                fish.transform = getTransform(tt1,theta,ver)
                
            elif i%4==1:
                fish.transform = getTransform(tt2,theta,ver)
                
            elif i%4==2:
                fish.transform = getTransform(tt3,theta,ver)
                
            elif i%4==3:
                fish.transform = getTransform(tt4,theta,ver)
            if x<numeroFish:
                x+=1

        
        if controller.leftClickOn:
            controller.leftClickOn = not controller.leftClickOn
            if numeroFish>0:
                nom_base = "fish_numero_" + str(numeroFish-1)
                nom = "t_fish_numero_" + str(numeroFish-1)
                cardumenTree = sg.findNode(nodeWorld, "cardumen")
                numberList = []
                for i in range(numeroFish):
                    nom_base = "fish_numero_" + str(i)
                    nom = "t_fish_numero_" + str(i)
                    comida = sg.findNode(cardumenTree, nom_base)
                    t5 = sg.findTransform(comida, nom , tr.identity())
                    number = [sg.findPosition(world, nom , tr.identity() )[0] +1.0,sg.findPosition(world, nom, tr.identity() )[1] +1.0]
                    numberList.append(number)

                controller.mouse_y = abs(2 - controller.mouse_y)

                for i in range(len(numberList)):
                    number = numberList[i]
                    if abs(controller.mouse_x - number[0]) < 0.2 and abs(controller.mouse_y - number[1] ) < 0.1:
                        cardumenTree.childs.pop()
                        numeroFish-=1
            else:
                print("AGREGUE MAS PECES PARA SEGUIR")

        bNode = sg.findNode(world, "burbujas")
        for i in range(10):
            bNode1 = sg.findNode(bNode, "burbble_" + str(i))
            bNode1.transform = getTransform(TR_UP,theta,0)


        # Drawing the Car
        sg.drawSceneGraphNode(world, shaderProgram, tr.identity())

        # Once the render is done, buffers are swapped, showing only the complete scene.
        glfw.swap_buffers(window)

    
    glfw.terminate()