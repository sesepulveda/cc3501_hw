#!/usr/bin/python
#recolectando info de otros lados
'''
	https://www.python-course.eu/sys_module.php

'''
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import sys
import os
import time
import functools 
import scipy.misc as smisc
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import spsolve
from scipy.sparse import lil_matrix #Row-based linked list sparse matrix

# or it can be iterated via a for loop:


def main():
	cmd = sys.argv[1:]
	# Command line atributes
	imgin = cmd[0] + ".png" 	#input
	imgout = cmd[1] + ".png"	#output
	entries = os.listdir()
	assert imgin in entries, "Input file not found" 

	# read image
	img = mpimg.imread(imgin)

	inn = explore(img)

	img = compare(img, inn, 0, False)
	
	imgplot = plt.imshow(img)

	mpimg.imsave(imgout,img) # sauvegarder le output celon son nom

	plt.show(imgplot)

def compare(img, inn, b, b2):
	if b==1:
		contador = 3
		tl1 = []
		while(contador>0):
			s1 = time.time()
			img = resolucion(img, inn[0], inn[1],b2)
			s2 = time.time()
			diff = s2-s1
			tl1.append(diff)
			contador-=1
		num = functools.reduce(lambda x,y: x+y, tl1)
		prom = num/len(tl1)
		print("finish time: " + str(prom))
		tl2 = []
		cont = 3
		while(cont>0):
			s3 = time.time()
			img = optimate(img, inn[0], inn[1],b2)
			s4 = time.time()
			diff = s4-s3
			tl2.append(diff)
			cont-=1
		num2 = functools.reduce(lambda x,y: x+y, tl2)
		prom2 = num2/len(tl2)
		print("finish time: " + str(prom2))
	elif b==0:
		img = optimate(img, inn[0], inn[1],b2)

	return img

def explore(img):
	incog = []
	connu = []
	# Extrapolando el problema a diferencias finitas de laplace dirichlet
	con = 0
	for i in range(len(img)):
		for j in range(len(img[0])):
			if img[i][j][0]==1.0:
				if img[i][j][1] == 1.0:
					if img[i][j][2] == 1.0:
						incog.append([i,j])
						ni = len(incog)-1
						if (j-1)>-1:
							if [i,j-1] not in incog:
								connu.append(img[i][j-1])
								con+=1
						if (i-1)>-1:
							if [i-1,j] not in incog:
								connu.append(img[i-1][j])
								con+=1
						if (j+1)<len(img):
							if any([v!=1.0 for v in img[i][j+1]]):
								connu.append(img[i][j+1])
								con+=1
						if (i+1)<len(img):
							if any([v!=1.0 for v in img[i+1][j]]):
								connu.append(img[i+1][j])
								con+=1
						if con>0:
							if len(img[i][j])==4:
								suma = np.array([.0,.0,.0,.0])
							elif len(img[i][j])==3:
								suma = np.array([.0,.0,.0])
							l = len(connu)-1
							while(l>ni):
								#print(connu[l])
								suma += connu.pop(l)
								l-=1
							connu[ni] = -1*(connu[ni]+suma)
						else:
							if len(img[i][j])==4:
								connu.append(-np.array([.0,.0,.0,1.0]))
							elif len(img[i][j])==3:
								connu.append(-np.array([.0,.0,.0]))
						con=0
	return [incog, connu]


def resolucion(img, incog, connu, bl):
	N = len(incog)
	#print("N de incognitas: " + str(N))
	A = np.zeros((N,N))
	contador = 0
	for d in range(len(incog)):
		i = incog[d][0]#fila
		j = incog[d][1]#columna
		A[d][d]=-4
		if i+1<len(img):
			if [i+1,j] in incog:
				ind = incog.index([i+1,j])
				A[d][ind]+=1
				contador+=1
		else:
			A[d][d]+=1
		if i-1>-1:
			if [i-1,j] in incog:
				ind = incog.index([i-1,j])
				A[d][ind]+=1
				contador+=1
		else:
			A[d][d]+=1
		if j+1<len(img[0]):
			if [i,j+1] in incog:
				ind = incog.index([i,j+1])
				A[d][ind]+=1
				contador+=1
		else:
			A[d][d]+=1
		if j-1>-1:
			if [i,j-1] in incog:
				ind = incog.index([i,j-1])
				A[d][ind]+=1
				contador+=1
		else:
			A[d][d]+=1

	relleno = contador+N
	#print("relleno matriz A: " +  str(relleno))

	zeros=(N**2 -  contador+N)
	#print("total de zeros: " + str(zeros) )
	total = N**2
	#print("total de elementos: "+ str(total))
	#print("razon: " + str(zeros / total))
	#print(A)
	if bl:
		data_size = A.nbytes/(1024)
		print('Size of full matrix with zeros: '+ '%3.2f' %data_size + ' KB')
	connu = np.asarray(connu)   

	x1 = np.linalg.solve(A, connu[:,0])
	x2 = np.linalg.solve(A, connu[:,1])
	x3 = np.linalg.solve(A, connu[:,2])

	for i in range(len(x1)):
		d1 = incog[i][0]
		d2 = incog[i][1]
		if len(img[0][0])==4:
			sol = [x1[i],x2[i],x3[i],1.0]
		elif len(img[0][0])==3:
			sol = [x1[i],x2[i],x3[i]]
		img[d1][d2] = sol

	return img

def optimate(img, incog, connu, bl):
	N = len(incog)
	#print("N de incognitas: " + str(N))
	data=[]
	row=[]
	col=[]
	for d in range(len(incog)):
		i = incog[d][0]#fila
		j = incog[d][1]#columna
		data.append(-4)
		pos = len(data) -1
		row.append(d)
		col.append(d)
		if i+1<len(img):
			if [i+1,j] in incog:
				ind = incog.index([i+1,j])
				data.append(1)
				row.append(d)
				col.append(ind)
		else:
			data[pos]+=1
		if i-1>-1:
			if [i-1,j] in incog:
				ind = incog.index([i-1,j])
				data.append(1)
				row.append(d)
				col.append(ind)
		else:
			data[pos]+=1
		if j+1<len(img[0]):
			if [i,j+1] in incog:
				ind = incog.index([i,j+1])
				data.append(1)
				row.append(d)
				col.append(ind)
		else:
			data[pos]+=1
		if j-1>-1:
			if [i,j-1] in incog:
				ind = incog.index([i,j-1])
				data.append(1)
				row.append(d)
				col.append(ind)
		else:
			data[pos]+=1

	aprima = csr_matrix((data, (row, col)), shape=(N, N))

	#ap = csr_matrix(A)

	if bl:
		data_csr_size = aprima.data.nbytes/(1024)
		print('Size of sparse csr_matrix: '+ '%3.2f' %data_csr_size + ' KB')

	connu = np.asarray(connu)    
	#print(connu[:,0])
	x1p = spsolve(aprima, connu[:,0])
	x2p = spsolve(aprima, connu[:,1])
	x3p = spsolve(aprima, connu[:,2])

	for i in range(len(x1p)):
		d1 = incog[i][0]
		d2 = incog[i][1]
		if len(img[0][0])==4:
			sol = [x1p[i],x2p[i],x3p[i],1.0]
		elif len(img[0][0])==3:
			sol = [x1p[i],x2p[i],x3p[i]]
		img[d1][d2] = sol 

	return img

def eightPoints(img):
	incog = []
	connu = []
	# Extrapolando el problema a diferencias finitas de laplace dirichlet
	con = 0
	for i in range(len(img)):
		for j in range(len(img[0])):
			#print(img[i][j])
			#add = [img[i][j] for v in img[i][j] if v==1.0]
			#incog.append(add)
			if img[i][j][0]==1.0:
				if img[i][j][1] == 1.0:
					if img[i][j][2] == 1.0:
						incog.append([i,j])
						ni = len(incog)-1
						if (j-1)>-1:
							if [i,j-1] not in incog:
								connu.append(img[i][j-1])
								con+=1
						if (i-1)>-1:
							if [i-1,j] not in incog:
								connu.append(img[i-1][j])
								con+=1
						if (i-1)>-1 and (j+1)<len(img) :
							if [i-1,j+1] not in incog:
								connu.append(img[i-1][j+1])
								con+=1
						if (i-1)>-1 and (j-1)>-1 :
							if [i-1,j-1] not in incog:
								connu.append(img[i-1][j-1])
								con+=1
						if (j+1)<len(img):
							if any([v!=1.0 for v in img[i][j+1]]):
								connu.append(img[i][j+1])
								con+=1
						if (i+1)<len(img):
							if any([v!=1.0 for v in img[i+1][j]]):
								connu.append(img[i+1][j])
								con+=1
						if (i+1)<len(img) and (j+1)<len(img) :
							if any([v!=1.0 for v in img[i+1][j+1]]):
								connu.append(img[i+1][j+1])
								con+=1
						if (i+1)<len(img) and (j-1)>-1 :
							if any([v!=1.0 for v in img[i+1][j-1]]):
								connu.append(img[i+1][j-1])
								con+=1
						if con>0:
							if len(img[i][j])==4:
								suma = np.array([.0,.0,.0,.0])
							elif len(img[i][j])==3:
								suma = np.array([.0,.0,.0])
							l = len(connu)-1
							while(l>ni):
								#print(connu[l])
								suma += connu.pop(l)
								l-=1
							connu[ni] = -1*(connu[ni]+suma)
						else:
							if len(img[i][j])==4:
								connu.append(-np.array([.0,.0,.0,1.0]))
							elif len(img[i][j])==3:
								connu.append(-np.array([.0,.0,.0]))
						con=0
	N = len(incog)
	#print("N de incognitas: " + str(N))
	A = np.zeros((N,N))
	mat = []
	contador = 0
	for d in range(len(incog)):
		i = incog[d][0]#fila
		j = incog[d][1]#columna
		A[d][d]=-8
		if [i+1,j] in incog:
			ind = incog.index([i+1,j])
			A[d][ind]+=1
			contador+=1
		if [i-1,j] in incog:
			ind = incog.index([i-1,j])
			A[d][ind]+=1
			contador+=1
		if [i,j+1] in incog:
			ind = incog.index([i,j+1])
			A[d][ind]+=1
			contador+=1
		if [i,j-1] in incog:
			ind = incog.index([i,j-1])
			A[d][ind]+=1
			contador+=1
		if [i+1,j-1] in incog:
			ind = incog.index([i+1,j-1])
			A[d][ind]+=1
			contador+=1
		if [i-1,j-1] in incog:
			ind = incog.index([i-1,j-1])
			A[d][ind]+=1
			contador+=1
		if [i+1,j+1] in incog:
			ind = incog.index([i+1,j+1])
			A[d][ind]+=1
			contador+=1
		if [i-1,j+1] in incog:
			ind = incog.index([i-1,j+1])
			A[d][ind]+=1
			contador+=1

	ap = csr_matrix(A)
	connu = np.asarray(connu)    

	x1p = spsolve(ap, connu[:,0])
	x2p = spsolve(ap, connu[:,1])
	x3p = spsolve(ap, connu[:,2])

	for i in range(len(x1p)):
		d1 = incog[i][0]
		d2 = incog[i][1]
		if len(img[0][0])==4:
			sol = [x1p[i],x2p[i],x3p[i],1.0]
		elif len(img[0][0])==3:
			sol = [x1p[i],x2p[i],x3p[i]]
		img[d1][d2] = sol 

	return img

if __name__ == '__main__':
	main()