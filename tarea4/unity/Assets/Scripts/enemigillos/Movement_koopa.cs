﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement_koopa : MonoBehaviour
{
    float speedX = -2;
    float speedY = 0;
    float speedZ = 0;

    // Start is called before the first frame update
    void Start()
    {   
    }
    // Update the speed in Koopa character
    void Update()
    {
        transform.Translate(new Vector3(speedX, speedY, speedZ) * Time.deltaTime);
    }
}
